"""
@author: Henning Wessels

"""


import numpy as np
import matplotlib.pyplot as plt
import math
import random


def plotInitialCondition(x, y, u0, idu1, testCase, BC_type):

    f, ax = plt.subplots(1)

    plt.scatter(x[idu1], y[idu1], c=u0[idu1], cmap='RdBu_r')
    cb = plt.colorbar()
    for l in cb.ax.yaxis.get_ticklabels():
        l.set_fontsize(20)

    ax.tick_params(axis='both', labelsize=20)

    plt.savefig('figures/' + testCase + '/' + BC_type + '.png')
    plt.close()


def staticPressure(testCase):

    # spatial discretization
    x_min  = 0.0
    x_max  = 1.0
    num_el = 500
    n_boun = 100     # boundary nodes per facet

    dx = 0.1/math.sqrt(num_el + n_boun)

    # space time domain
    x = np.random.uniform(dx, (1-dx), num_el).flatten()[:, None]
    x = np.sort(x, axis=0)
    y = np.random.uniform(dx, (1-dx), num_el).flatten()[:, None]

# boundary coordinates

    n_facets = 4

    '''1st boundary facet: u(x=0, y) = 1'''
    x1_x       = np.zeros((n_boun, 1))
    y1_x       = np.zeros((n_boun, 1))
    y1_x[:,0]  = np.random.uniform(0, 1, n_boun)
    u1_x       = np.zeros((n_boun, 1))

    idu1_x     = np.arange(0, n_boun, 1)     # index for later assemby of initial solution

    '''2nd boundary facet: u(x=1, y) = 0'''
    x2_x       = np.ones((n_boun, 1))
    y2_x       = np.zeros((n_boun, 1))
    y2_x[:, 0] = np.random.uniform(0, 1, n_boun)
    u2_x       = np.zeros((n_boun, 1))

    # append to x1, y1, u1
    x1_x       = np.append(x1_x, x2_x, axis=0)
    y1_x       = np.append(y1_x, y2_x, axis=0)
    u1_x       = np.append(u1_x, u2_x, axis=0)

    idu1_x     = np.append(idu1_x, np.arange(n_boun, 2*n_boun, 1, dtype=np.int64)) # index for later assemby of initial solution

    '''3rd boundary facet: u(x, y=0) = 1'''
    x1_y       = np.zeros((n_boun, 1))
    x1_y[:, 0] = np.random.uniform(0, 1, n_boun)
    y1_y       = np.zeros((n_boun, 1))
    u1_y       = np.zeros((n_boun, 1))

    idu1_y     = np.arange(2*n_boun, 3*n_boun, 1)

    '''4th boundary facet: u(x, y=1) = 0 '''
    x1_p       = np.zeros((n_boun, 1))
    x1_p[:, 0] = np.random.uniform(0, 1, n_boun)
    y1_p       = np.ones((n_boun, 1))
    u1_p       = np.zeros((n_boun, 1))

    idu1_p     = np.arange(3*n_boun, 4*n_boun, 1)

    # append to x1, y1, u1
    x = np.concatenate([x1_x, x1_y, x1_p, x], axis=0).flatten()[:, None]
    y = np.concatenate([y1_x, y1_y, y1_p, y], axis=0).flatten()[:, None]

    '''Upper and lower bounds'''
    lb = np.array([np.min(x), np.min(y)])
    ub = np.array([np.max(x), np.max(y)])

    # initialization
    T = np.zeros((3, len(x)))
    T[0, idu1_x] = u1_x[:, 0]
    T[1, idu1_y] = u1_y[:, 0]
    T[2, idu1_p] = u1_p[:, 0]


    '''Indices of free nodes'''
    id_free_x = np.arange(0, num_el+n_boun*n_facets)
    id_free_x = np.delete(id_free_x, idu1_x)

    id_free_y = np.arange(0, num_el+n_boun*n_facets)
    id_free_y = np.delete(id_free_y, idu1_y)

    id_free_p = np.arange(0, num_el+n_boun*n_facets)
    id_free_p = np.delete(id_free_p, idu1_p)

    '''plot initial condition'''
    plotInitialCondition(x, y, T[0, :], idu1_x, testCase, 'x-boun')
    plotInitialCondition(x, y, T[1, :], idu1_y, testCase, 'y-boun')
    plotInitialCondition(x, y, T[2, :], idu1_p, testCase, 'p-boun')

    return id_free_x, idu1_x, id_free_y, idu1_y, id_free_p, idu1_p, lb, ub, x, y, T