"""
@author: Henning Wessels

This code is based on the work of Maziar Raissi.
For execution, the folder 'Utilities' must be downloaded from
https://github.com/maziarraissi/PINNs/

"""

import sys
sys.path.insert(0, '/home/wessels/Utilities/')
# sys.path.insert(0, '/global/')



import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import time
import scipy.io
from plotting import newfig, savefig
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import multiprocessing

# private functions
import initialization as init


np.random.seed(1234)
tf.compat.v1.set_random_seed(1234)

def plot(x, y, U_plot, filename, testCase, lb, ub, t_step):

    '''Generic formatting'''

    cbformat = ticker.ScalarFormatter()  # create the formatter
    cbformat.set_powerlimits((-2, 2))

    f, ax = plt.subplots(1)

    plt.scatter(x, y, c=U_plot, cmap='RdBu_r')
    plt.xlabel(r'$x$', fontsize=20)
    plt.ylabel(r'$y$', fontsize=20)

    cb = plt.colorbar(format=cbformat)

    if(filename=='pressure_t'):
        plt.clim(0, 10)
        print('min p = %.8e' % (np.min(U_plot)))
        print('max p = %.8e' % (np.max(U_plot)))
        cb.set_label(label=r'$p$', fontsize=20)
    elif(filename=='x-velocity_t'):
        cb.set_label(label=r'$v_x$', fontsize=20)
    elif(filename=='y-velocity_t'):
        cb.set_label(label=r'$v_y$', fontsize=20)
    elif(filename == 'velocity_magnitude_t'):
        cb.set_label(label=r'$\|\mathbf{v}\|$', fontsize=20)
    elif(filename=='divergence_t'):
        cb.set_label(label=r'$\|\mbox{div}\,\mathbf{v}\|$', fontsize=20)
    elif(filename=='x-dist-net_t'):
        cb.set_label(label=r'$D_x$', fontsize=20)
    elif (filename == 'y-dist-net_t'):
        cb.set_label(label=r'$D_y$', fontsize=20)

    for l in cb.ax.yaxis.get_ticklabels():
        # l.set_weight("bold")
        l.set_fontsize(20)

    ax.tick_params(axis='both', labelsize=20)
    major_ticks = np.arange(lb[0], ub[0] + 1e-10, 0.5)
    ax.set_xticks(major_ticks)
    ax.set_yticks(major_ticks)

    plt.gcf().subplots_adjust(bottom=0.2)
    # plt.gcf().subplots_adjust(left=0.0)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 1.0, box.height])

    plt.savefig('figures/' + testCase + '/' + filename + str(t_step) + '.png')
    plt.close()



class PhysicsInformedNN:
    # Initialize the class
    def __init__(self, id_free_x, idu1_x, id_free_y, idu1_y, id_free_p, idu1_p, x0, y0, rho0, u0, layers, dt, lb, ub, q, mu, testCase, t_step):

        'LOAD DATA'''

        self.testCase = testCase
        self.t_step = t_step

        # fixed and free node indices
        self.idu1_x = idu1_x
        self.idu1_y = idu1_y
        self.idu1_p = idu1_p

        # material data
        self.rho0  = rho0
        self.mu    = mu

        # gravity acceleration
        self.b_x = 0.0
        self.b_y = -10.0

        # lower and upper bound
        self.lb = lb
        self.ub = ub

        # initial coordinates and solution
        self.x0 = x0                               # initial x-position
        self.y0 = y0                               # initial y-position

        # domain height and width for distance function
        self.width  = np.max(x0) - np.min(x0)
        self.height = np.max(y0) - np.min(y0)

        print('width = %.3e'  % (self.width))
        print('height = %.3e' % (self.height))

        self.u0_x = u0[:, 0].flatten()[:, None]    # initial x-velocity
        self.u0_y = u0[:, 1].flatten()[:, None]    # initial y-velocity

        # boundary velocity x-direction
        self.x1_x = x0[idu1_x]    # boundary x-position
        self.y1_x = y0[idu1_x]    # boundary y-position
        self.u1_x = u0[idu1_x, 0:1]    # boundary solution

        # boundary velocity y-direction
        self.x1_y = x0[idu1_y]    # boundary x-position
        self.y1_y = y0[idu1_y]    # boundary y-position
        self.u1_y = u0[idu1_y, 1:2]    # boundary solution

        # boundary pressure
        self.x1_p = x0[idu1_p]    # boundary x-position
        self.y1_p = y0[idu1_p]    # boundary y-position
        # self.u1_p = u0[idu1_p, 2:3]    # boundary solution

        # others
        self.layers = layers
        self.dt = dt
        self.q = max(q,1)
    
        # Initialize NN
        self.weights, self.biases = self.initialize_NN(layers)
        
        # Load IRK weights
        tmp = np.float32(np.loadtxt('/home/wessels/Utilities/IRK_weights/Butcher_IRK%d.txt' % (q), ndmin = 2))
        self.IRK_weights = np.reshape(tmp[0:q**2+q], (q+1,q))
        self.IRK_times = tmp[q**2+q:]

        # graph
        self.sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(allow_soft_placement=True))
                                                     # , log_device_placement=True))

        'PLACEHOLDERS'

        # placeholder for initial position
        self.x0_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.x0.shape[1]), name='x0_tf')
        self.y0_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.y0.shape[1]), name='y0_tf')

        # placeholder for initial density
        self.rho0_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.rho0.shape[1]), name='rho0_tf')

        # placeholder for boundary coordinates
        self.x1_x_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.x1_x.shape[1]), name='x1_x_tf')
        self.y1_x_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.y1_x.shape[1]), name='y1_x_tf')

        self.x1_y_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.x1_y.shape[1]), name='x1_y_tf')
        self.y1_y_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.y1_y.shape[1]), name='y1_y_tf')

        self.x1_p_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.x1_p.shape[1]), name='x1_p_tf')
        self.y1_p_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.y1_p.shape[1]), name='y1_p_tf')

        # placeholder for concatenated boundary coordinates
        self.x1_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.x1_x.shape[1]), name='x1_tf')
        self.y1_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.y1_x.shape[1]), name='y1_tf')

        # placeholder for initial condition
        self.u0_x_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.u0_x.shape[1]), name='u0_x_tf')
        self.u0_y_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.u0_y.shape[1]), name='u0_y_tf')

        # placeholder for boundary condition
        self.u1_x_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.u1_x.shape[1]), name='u1_x_tf')
        self.u1_y_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.u1_y.shape[1]), name='u1_y_tf')

        # dummy variables for velocity gradients
        self.dummy_x0_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.q), name='dummy_x0_tf') # dummy variable for fwd_gradients
        self.dummy_x0_p_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, 1), name='dummy_x0_p_tf') # dummy variable for pressure gradient
        self.dummy_x1_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.q+1), name='dummy_x1_tf') # dummy variable for fwd_gradients

        self.U0_x_pred, self.U0_y_pred, self.divU = self.balance_equations(self.x0_tf, self.y0_tf, self.rho0_tf) # N x 2*(q+1)+1
        self.U1_x_out, self.U1_y_out, self.divU_out, self.detF_out, self.x_out, self.y_out, self.p_out = self.output_solution(self.x1_tf, self.y1_tf)          # N1 x 2*(q+1)+1
        self.U1_p_pred = self.net_U1_p(self.x1_p_tf, self.y1_p_tf)

        '''Initial pressure not penalized!!!'''

        self.loss = tf.reduce_sum(tf.square(self.U1_p_pred)) \
                     + tf.reduce_sum(tf.square(self.u0_x_tf - self.U0_x_pred)) \
                     + tf.reduce_sum(tf.square(self.u0_y_tf - self.U0_y_pred)) \
                     + tf.reduce_sum(tf.square(self.divU))

        self.optimizer = tf.contrib.opt.ScipyOptimizerInterface(self.loss,
                                                                method = 'L-BFGS-B',
                                                                options = {'maxiter': 1000000,
                                                                           'maxfun': 50000,
                                                                           'maxcor': 50,
                                                                           'maxls': 50,
                                                                           'ftol' : 1.0 * np.finfo(float).eps})

        self.optimizer_Adam = tf.compat.v1.train.AdamOptimizer(learning_rate=0.0001)
        self.train_op_Adam = self.optimizer_Adam.minimize(self.loss)

        self.saver = tf.compat.v1.train.Saver()

        init = tf.compat.v1.global_variables_initializer()
        self.sess.run(init)


        
    def initialize_NN(self, layers):        
        weights = []
        biases = []
        num_layers = len(layers) 
        for l in range(0,num_layers-1):
            W = self.xavier_init(size=[layers[l], layers[l+1]])
            b = tf.Variable(tf.zeros([1,layers[l+1]], dtype=tf.float32), dtype=tf.float32)
            weights.append(W)
            biases.append(b)        
        return weights, biases
        
    def xavier_init(self, size):
        in_dim = size[0]
        out_dim = size[1]        
        xavier_stddev = np.sqrt(2/(in_dim + out_dim))
        return tf.Variable(tf.random.truncated_normal([in_dim, out_dim], stddev=xavier_stddev), dtype=tf.float32)
    
    def neural_net(self, X, weights, biases):
        num_layers = len(weights) + 1
        
        H = 2.0*(X - self.lb)/(self.ub - self.lb) - 1.0
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.tanh(tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y
    
    def fwd_gradients_x0(self, U, x):
        g = tf.gradients(U, x, grad_ys=self.dummy_x0_tf)[0]
        return tf.gradients(g, self.dummy_x0_tf)[0]

    def fwd_gradients_x0_p(self, U, x):
        g = tf.gradients(U, x, grad_ys=self.dummy_x0_p_tf)[0]
        return tf.gradients(g, self.dummy_x0_p_tf)[0]

    def fwd_gradients_x1(self, U, x):
        g = tf.gradients(U, x, grad_ys=self.dummy_x1_tf)[0]
        return tf.gradients(g, self.dummy_x1_tf)[0]

    def balance_equations(self, X, Y, RHO_0):

        ''''''
        '''Distance function and boundary extension'''
        D_x    = -(4/(self.width**2))*X**2 + (4/self.width)*X
        D_x_x  = -(8/(self.width**2))*X + (4/self.width)

        G_x    = 0
        G_x_x  = 0

        D_y    = Y/self.height
        D_y_y  = 1/self.height

        G_y    = 0
        G_y_y  = 0

        '''Neural network'''
        U1 = self.neural_net(tf.concat([X,Y], 1), self.weights, self.biases)

        '''RK velocity stages & solution'''

        U1_hat_x = U1[:, 0:self.q+1]
        U1_hat_y = U1[:, (self.q+1):(2*self.q+2)]

        U1_x  = G_x + D_x*U1[:, 0:(self.q+1)]
        U1_y  = G_y + D_y*U1[:, (self.q+1):(2*self.q + 2)]



        '''Pressure RK stages'''
        p_stages   = U1[:, (2*self.q+2):(3*self.q+2)]      # RK pressure stages


        '''Grad v'''
        dot_F_11 = D_x*self.fwd_gradients_x1(U1_hat_x, X) + D_x_x*U1_hat_x + G_x_x
        dot_F_12 = D_x*self.fwd_gradients_x1(U1_hat_x, Y)

        dot_F_21 = D_y*self.fwd_gradients_x1(U1_hat_y, X)
        dot_F_22 = D_y*self.fwd_gradients_x1(U1_hat_y, Y) + D_y_y*U1_hat_y + G_y_y

        '''Delta F'''
        F_11 = self.dt*tf.matmul(dot_F_11[:, 0:self.q], self.IRK_weights.T) + 1
        F_12 = self.dt*tf.matmul(dot_F_12[:, 0:self.q], self.IRK_weights.T)

        F_21 = self.dt*tf.matmul(dot_F_21[:, 0:self.q], self.IRK_weights.T)
        F_22 = self.dt*tf.matmul(dot_F_22[:, 0:self.q], self.IRK_weights.T) + 1

        '''Inverse of deformation gradient'''
        detF = tf.multiply(F_11, F_22) - tf.multiply(F_12, F_21)

        F_inv_11 =  tf.divide(F_22, detF)
        F_inv_12 = -tf.divide(F_12, detF)

        F_inv_21 = -tf.divide(F_21, detF)
        F_inv_22 =  tf.divide(F_11, detF)

        '''!!! Momentum equation: only stages go into acceleration computation!!'''

        '''Density inverse'''
        invRho = tf.divide(1.0, RHO_0)

        '''Grad p'''
        Grad_p_X = self.fwd_gradients_x0(-p_stages, X)
        Grad_p_Y = self.fwd_gradients_x0(-p_stages, Y)

        grad_p_x = tf.multiply(F_inv_11[:, 0:self.q], Grad_p_X) + tf.multiply(F_inv_21[:, 0:self.q], Grad_p_Y)
        grad_p_y = tf.multiply(F_inv_12[:, 0:self.q], Grad_p_X) + tf.multiply(F_inv_22[:, 0:self.q], Grad_p_Y)

        '''Momentum equation in x and y'''
        acce_x = self.b_x + tf.multiply(invRho, grad_p_x)
        acce_y = self.b_y + tf.multiply(invRho, grad_p_y)

        '''Time integration''' # v_n_i = v_n+1 - dt*sum_i a_ji f(xi_i) \\ v_n_q+1 = v_n+1 - dt*sum_j b_j f(xi_j)
        U0_x = U1_x - self.dt*tf.matmul(acce_x, self.IRK_weights.T)
        U0_y = U1_y - self.dt*tf.matmul(acce_y, self.IRK_weights.T)


        '''!!! Mass balance'''

        '''Spatial velocity gradient l'''
        l_11 = tf.multiply(dot_F_11, F_inv_11) + tf.multiply(dot_F_12, F_inv_21)
        l_22 = tf.multiply(dot_F_21, F_inv_12) + tf.multiply(dot_F_22, F_inv_22)

        divU = l_11 + l_22


        return U0_x, U0_y, divU

    def output_solution(self, X, Y):

        ''''''
        '''Distance function and boundary extension'''
        D_x    = -(4/(self.width*self.width))*X*X + (4/self.width)*X
        D_x_x  = -(8/(self.width**2))*X    + (4/self.width)

        G_x    = 0
        G_x_x  = 0

        D_y    = Y/self.height
        D_y_y  = 1/self.height

        G_y    = 0
        G_y_y  = 0

        '''Neural network'''
        U1 = self.neural_net(tf.concat([X ,Y], 1), self.weights, self.biases)

        '''RK velocity stages & solution'''
        U1_hat_x = U1[:, 0:(self.q + 1)]
        U1_hat_y = U1[:, (self.q + 1):(2 * self.q + 2)]

        U1_x  = G_x + D_x*U1_hat_x
        U1_y  = G_y + D_y*U1_hat_y

        '''Pressure RK stages'''
        p_stages = U1[:, (2*self.q+2):(3*self.q+2)]      # RK pressure stages

        '''Updated Position and pressure'''
        b_j  = self.IRK_weights[self.q:(self.q+1),:].T
        x    = X + self.dt*tf.matmul(U1_x[:, 0:self.q], b_j)
        y    = Y + self.dt*tf.matmul(U1_y[:, 0:self.q], b_j)
        p_na = tf.matmul(p_stages, b_j)

        '''Grad v'''
        dot_F_11 = D_x*self.fwd_gradients_x1(U1_hat_x, X) + G_x_x + D_x_x*U1_hat_x
        dot_F_12 = D_x*self.fwd_gradients_x1(U1_hat_x, Y)

        dot_F_21 = D_y*self.fwd_gradients_x1(U1_hat_y, X)
        dot_F_22 = D_y*self.fwd_gradients_x1(U1_hat_y, Y) + G_y_y + D_y_y*U1_hat_y

        '''Delta F'''
        F_11 = self.dt*tf.matmul(dot_F_11[:, 0:self.q], b_j) + 1
        F_12 = self.dt*tf.matmul(dot_F_12[:, 0:self.q], b_j)

        F_21 = self.dt*tf.matmul(dot_F_21[:, 0:self.q], b_j)
        F_22 = self.dt*tf.matmul(dot_F_22[:, 0:self.q], b_j) + 1

        '''Inverse of deformation gradient'''
        detF = tf.multiply(F_11, F_22) - tf.multiply(F_12, F_21)

        F_inv_11 =  tf.divide(F_22, detF)
        F_inv_12 = -tf.divide(F_12, detF)

        F_inv_21 = -tf.divide(F_21, detF)
        F_inv_22 =  tf.divide(F_11, detF)

        '''Spatial velocity gradient l'''
        l_11 = tf.multiply(dot_F_11[:, self.q:(self.q+1)], F_inv_11) + tf.multiply(dot_F_12[:, self.q:(self.q+1)], F_inv_21)
        l_22 = tf.multiply(dot_F_21[:, self.q:(self.q+1)], F_inv_12) + tf.multiply(dot_F_22[:, self.q:(self.q+1)], F_inv_22)

        '''div v = tr l'''

        divU = l_11 + l_22

        return U1_x, U1_y, divU, detF, x, y, p_na # N x (2*(q+1) +1)

    def net_U1_p(self, x1_p, y1_p):

        U1 = self.neural_net(tf.concat([x1_p, y1_p], 1), self.weights, self.biases)
        U1_p = U1[:, 2*(self.q+1):3*(self.q+1)]

        return U1_p # N x (2*(q+1) +1)

    def callback(self, loss):
        print('Loss:', loss)
    
    def train(self, nIter):
        tf_dict = {self.x0_tf: self.x0, self.y0_tf: self.y0,
                   self.u0_x_tf: self.u0_x, self.u0_y_tf: self.u0_y,
                   self.rho0_tf: self.rho0,
                   self.x1_x_tf: self.x1_x, self.y1_x_tf: self.y1_x,
                   self.x1_y_tf: self.x1_y, self.y1_y_tf: self.y1_y,
                   self.x1_p_tf: self.x1_p, self.y1_p_tf: self.y1_p,
                   self.x1_tf: np.zeros((self.x0.shape[0], 1)),
                   self.y1_tf: np.zeros((self.y0.shape[0], 1)),
                   self.u1_x_tf: self.u1_x, self.u1_y_tf: self.u1_y, #self.u1_p_tf: self.u1_p,
                   self.dummy_x0_tf: np.ones((self.x0.shape[0], self.q)),
                   self.dummy_x0_p_tf: np.ones((self.x0.shape[0], 1)),
                   self.dummy_x1_tf: np.ones((self.x0.shape[0], self.q+1))}
        
        start_time = time.time()
        for it in range(nIter):
            self.sess.run(self.train_op_Adam, tf_dict)
            
            # Print
            if it % 10 == 0:
                elapsed = time.time() - start_time
                loss_value = self.sess.run(self.loss, tf_dict)
                print('It: %d, Loss: %.3e, Time: %.2f' %
                      (it, loss_value, elapsed))
                start_time = time.time()

        self.optimizer.minimize(self.sess,
                                feed_dict = tf_dict,
                                fetches = [self.loss], loss_callback = self.callback)

    
    def predict(self, x_star, y_star):

        U1_x, U1_y, divU_out, detF_out, x_out, y_out, p_out = self.sess.run([self.U1_x_out, self.U1_y_out, self.divU_out, self.detF_out, self.x_out, self.y_out, self.p_out], {self.x1_tf: x_star, self.y1_tf: y_star, self.dummy_x0_p_tf: np.ones((self.x0.shape[0], 1)),
                                                                                                                                                            self.dummy_x1_tf: np.ones((self.x0.shape[0], self.q+1))})
                    
        return U1_x, U1_y, divU_out, detF_out, x_out, y_out, p_out

    def save(self):
        self.saver.save(self.sess, 'tmp/' + str(self.t_step) + '/' + self.testCase)

    def loadModel(self):
        self.saver.restore(self.sess, 'tmp/' + str(self.t_step) + '/' + self.testCase)


def run_tensorflow():

    testCase    = 'staticPressure'

    '''Network layout'''
    q = 8
    layers = [2, 60, (3*(q+1)-1)]

    '''Read time step from file'''
    idt = np.load('IC_BC/' + testCase + '/timeStep.npy')
    dt  = idt[0]
    t_step = int(idt[1])
    idt = np.delete(idt, 1)
    np.save('IC_BC/' + testCase + '/timeStep.npy', idt)

    '''Read model parameter from file'''
    IC = np.load('IC_BC/' + testCase + '/IC_t' + str(t_step-1) +'.npy')
    BC = np.load('IC_BC/' + testCase + '/BC_t0.npy', allow_pickle=True)

    '''Initial conditions'''
    x    = IC[:, 0].flatten()[:, None]
    y    = IC[:, 1].flatten()[:, None]
    u0   = IC[:, 2:5]
    rho0 = IC[:, 5].flatten()[:, None]

    mu   = 0.01    # dynamic viscosity

    '''Boundary conditions'''
    id_u1_x   = BC[0]
    id_u1_y   = BC[1]
    id_u1_p   = BC[2]
    id_free_x = BC[3]
    id_free_y = BC[4]
    id_free_p = BC[5]

    '''Lower and upper bounds'''
    lb = np.array([np.min(x), np.min(y)])
    ub = np.array([np.max(x), np.max(y)])

    '''build model'''
    start_time = time.time()
    model = PhysicsInformedNN(id_free_x, id_u1_x, id_free_y, id_u1_y, id_free_p, id_u1_p, x, y, rho0, u0, layers, dt, lb,
                              ub, q, mu, testCase, t_step)

    # train or load model
    adamSteps = 8000
    model.train(adamSteps)

    # predict
    U1_x, U1_y, divU_out, detF, x_out, y_out, P_plot = model.predict(x, y)

    # error in incompressibility
    error_incomp = np.linalg.norm(divU_out, 2)

    # print elapsed time
    elapsed_time = time.time() - start_time
    print('Error: %e \t Total elapsed: %e' % (error_incomp, elapsed_time))

    '''Update solution'''
    U_x_plot[:, 0] = np.transpose(U1_x[:, q])
    U_y_plot[:, 0] = np.transpose(U1_y[:, q])

    u0[:, 0] = U_x_plot[:, 0]
    u0[:, 1] = U_y_plot[:, 0]
    u0[:, 2] = P_plot[:, 0]

    # if(np.linalg.norm(u0[id_u1_x, 0] != 0.0)):
    #     print('ERROR x boundary\n')
    #     for i in range(0,len(id_u1_x)):
    #         print(u0[id_u1_x[i], 0], x[id_u1_x[i]])
    #
    # if(np.linalg.norm(u0[id_u1_y, 1] != 0.0)):
    #     print('ERROR y boundary \n')
    #     for i in range(0,len(id_u1_y)):
    #         print(u0[id_u1_y[i], 1], y[id_u1_y[i]])
    #     # print(y)

    '''Update coordinates (x,y) and density'''
    x[:] = x_out[:]
    y[:] = y_out[:]

    '''Plot field'''
    U_mag_plot = np.sqrt(np.square(U_x_plot) + np.square(U_y_plot))

    plot(x, y, P_plot, 'pressure_t', testCase, lb, ub, t_step)
    plot(x, y, U_x_plot, 'x-velocity_t', testCase, lb, ub, t_step)
    plot(x, y, U_y_plot, 'y-velocity_t', testCase, lb, ub, t_step)
    plot(x, y, U_mag_plot, 'y-velocity_t', testCase, lb, ub, t_step)
    plot(x[id_free_x], y[id_free_x], divU_out[id_free_x], 'divergence_t', testCase, lb, ub, t_step)

    '''Save result to file'''
    IC = np.concatenate((x, y, u0, rho0, P_plot), axis=1)
    np.save('IC_BC/' + testCase + '/IC_t' + str(t_step) +'.npy', IC)



if __name__ == "__main__": 

    testCase    = 'staticPressure'

    # time increment and number of steps
    dt = 1.0
    t_nn_steps = 50
    idt = np.arange(1,t_nn_steps+2,1, dtype=np.float32)
    idt = np.insert(idt, 0, dt)
    np.save('IC_BC/' + testCase + '/timeStep.npy', idt)

    # Initial and boundary conditions
    id_free_x, id_u1_x, id_free_y, id_u1_y, id_free_p, id_u1_p, lb, ub, x, y, Exact = init.staticPressure(testCase)
    u0 = Exact.T

    # material parameter
    rho0 = np.ones(len(x)).flatten()[:, None]      # density

    '''Save initial data to file'''
    IC = np.concatenate((x, y, u0, rho0, rho0), axis=1)
    BC = np.array([id_u1_x, id_u1_y, id_u1_p, id_free_x, id_free_y, id_free_p], dtype=object)

    np.save('IC_BC/' + testCase + '/IC_t0.npy', IC)
    np.save('IC_BC/' + testCase + '/BC_t0.npy', BC)

    '''plot initial data'''
    U_x_plot = np.zeros((len(y), 1))
    U_y_plot = np.zeros((len(y), 1))
    P_plot   = np.ones((len(y), 1))

    U_mag_plot = np.sqrt(np.square(U_x_plot) + np.square(U_y_plot))

    # plot
    plot(x, y, P_plot, 'pressure_t', testCase, lb, ub, 0)
    plot(x, y, U_x_plot, 'x-velocity_t', testCase, lb, ub, 0)
    plot(x, y, U_y_plot, 'y-velocity_t', testCase, lb, ub, 0)
    plot(x, y, U_mag_plot, 'velocity_magnitude_t', testCase, lb, ub, 0)
    plot(x, y, P_plot, 'divergence_t', testCase, lb, ub, 0)

    # start time
    start_time = time.time()

    '''Analytical solution for sloshing test case'''
    sloshingAmplitude = np.zeros((1, 3))
    if testCase=='sloshingIncomp':
        sloshingAmplitude = sloshing.get_amplitude(x, y, id_u1_p, sloshingAmplitude, dt, 0, testCase) # initial amplitude


    '''Loop over time steps'''
    for t_step in range(1, t_nn_steps+1):

        p = multiprocessing.Process(target=run_tensorflow)
        p.start()
        p.join()



