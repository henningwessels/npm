"""
@author: Henning Wessels

Postprocessing: Projection of discrete pressure onto a background mesh.

"""
import numpy as np
import matplotlib.pyplot as plt
import math
import tensorflow as tf
import sys


from scipy.interpolate import griddata
import matplotlib.ticker as ticker




testCase = 'staticPressure'
t_step   = 50

'''Read data'''

IC = np.load('IC_BC/' + testCase + '/IC_t' + str(t_step) + '.npy')
# BC = np.load('IC_BC/' + testCase + '/BC_t0.npy', allow_pickle=True)


X = IC[:, 0].flatten()[:, None]
Y = IC[:, 1].flatten()[:, None]
pressure = IC[:, 4]

points = np.concatenate([X, Y], axis=1)

'''set grid'''
grid_x, grid_y = np.mgrid[0:1:200j, 0:1:200j]

'''Plot interpolation'''
f, ax = plt.subplots(1)


grid_u0 = griddata(points, pressure, (grid_x, grid_y), method='linear')

plt.imshow(grid_u0.T, extent=(0,1,0,1), origin='lower', cmap='RdBu_r')
cb = plt.colorbar()
cb.set_label(label=r'$p$', fontsize=20)
cb.ax.tick_params(labelsize=20)
plt.clim(0,10)

ax.tick_params(axis='both', labelsize=20)
major_ticks = np.arange(0, 1 + 1e-10, 0.5)
ax.set_xticks(major_ticks)
ax.set_yticks(major_ticks)


plt.scatter(X, Y, s=40, facecolors='none', edgecolors='black')

plt.xlabel(r'$x$', fontsize=20)
plt.ylabel(r'$y$', fontsize=20)



plt.gcf().subplots_adjust(bottom=0.2)
# plt.gcf().subplots_adjust(left=0.0)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 1.0, box.height])

plt.grid()




plt.savefig('figures/' + testCase + '/grid_t'+ str(t_step) + '.png')

