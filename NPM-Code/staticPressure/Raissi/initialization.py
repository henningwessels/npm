"""
@author: Henning Wessels

"""


import numpy as np
import matplotlib.pyplot as plt
import math
import random



def plotInitialCondition(x, y, u0, idu1, testCase, BC_type):

    f, ax = plt.subplots(1)

    plt.scatter(x[idu1], y[idu1], c=u0[idu1], cmap='RdBu_r')
    cb = plt.colorbar()
    for l in cb.ax.yaxis.get_ticklabels():
        # l.set_weight("bold")
        l.set_fontsize(20)

    ax.tick_params(axis='both', labelsize=20)

    plt.savefig('figures/' + testCase + '/' + BC_type + '.png')
    plt.close()


def staticPressure_meshgrid(testCase):


    # spatial discretization
    x_min  = 0.0
    x_max  = 1.0
    y_min  = 0.0
    y_max  = 1.0

    nx, ny = (30, 30)
    x_tmp = np.linspace(x_min, x_max, nx)
    y_tmp = np.linspace(y_min, y_max, ny)
    x, y = np.meshgrid(x_tmp, y_tmp)

    x = x.flatten()[:, None]
    y = y.flatten()[:, None]

    '''top boundary'''
    idu1_top = np.argwhere(y==1)[:, 0]
    id_del   = np.concatenate([np.argwhere(x[idu1_top]==x_min)[:, 0], np.argwhere(x[idu1_top]==x_max)[:, 0]])
    idu1_top = np.delete(idu1_top, id_del)

    '''left boundary'''
    idu1_left = np.argwhere(x==0)[:, 0]

    '''right boundary'''
    idu1_right = np.argwhere(x==1)[:, 0]

    '''bottom boundary'''
    idu1_bottom = np.argwhere(y==0)[:, 0]

    '''Fixed indices'''
    idu1_p = idu1_top
    idu1_x = np.concatenate([idu1_left, idu1_right], axis=0)
    idu1_y = idu1_bottom

    '''Free indices'''
    id_free_x = np.arange(0, nx*ny)
    id_free_x = np.delete(id_free_x, idu1_x)

    id_free_y = np.arange(0, nx*ny)
    id_free_y = np.delete(id_free_y, idu1_y)

    id_free_p = np.arange(0, nx*ny)
    id_free_p = np.delete(id_free_p, idu1_p)

    '''Upper and lower bounds'''
    lb = np.array([np.min(x), np.min(y)])
    ub = np.array([np.max(x), np.max(y)])

    '''Initial values'''
    T = np.zeros((3, len(x)))

    '''plot initial condition'''
    plotInitialCondition(x, y, T[0, :], idu1_x, testCase, 'x-boun')
    plotInitialCondition(x, y, T[1, :], idu1_y, testCase, 'y-boun')
    plotInitialCondition(x, y, T[2, :], idu1_p, testCase, 'p-boun')

    return id_free_x, idu1_x, id_free_y, idu1_y, id_free_p, idu1_p, lb, ub, x, y, T