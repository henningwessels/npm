"""
@author: Henning Wessels

This code is based on the work of Maziar Raissi.
For execution, the folder 'Utilities' must be downloaded from
https://github.com/maziarraissi/PINNs/

"""

import sys
sys.path.insert(0, '/home/wessels/Utilities/')

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import time
import scipy.io


np.random.seed(1234)
tf.set_random_seed(1234)

def vibration(omega_0, D, omega_d, q_hat):

    # temporal discretization
    t = np.arange(0, 20, 1e-2)

    # solution
    q = q_hat*np.exp(-D*omega_0*t)*np.cos(omega_d*t)

    return q, t


class PhysicsInformedNN:
    # Initialize the class
    def __init__(self, x0, u0, layers, dt, omega_0, D, omega_d, q):


        # material parameter
        self.omega_0 = omega_0
        self.D       = D
        self.omega_d = omega_d

        self.q = max(q, 1)

        # lower and upper bound
        # self.lb = lb
        # self.ub = ub
        
        self.x0 = x0    # initial position
        self.u0 = u0    # initial solution
        
        self.layers = layers
        self.dt = dt
    
        # Initialize NN
        self.weights, self.biases = self.initialize_NN(layers)
        
        # Load IRK weights
        tmp = np.float32(np.loadtxt('/home/wessels/Utilities/IRK_weights/Butcher_IRK%d.txt' % (q), ndmin = 2))
        self.IRK_weights = np.reshape(tmp[0:q**2+q], (q+1,q))
        self.IRK_times = tmp[q**2+q:]
        
        # tf placeholders and graph
        self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
        
        self.x0_tf = tf.placeholder(tf.float32, shape=(None, self.x0.shape[1]), name='x0_tf')
        self.u0_tf = tf.placeholder(tf.float32, shape=(None, self.u0.shape[1]), name='u0_tf')

        self.U0_pred = self.net_U0(self.x0_tf) # N x (q+1)
        self.U1_pred, self.q_na = self.net_U1(self.x0_tf)
        
        self.loss = tf.reduce_sum(tf.square(self.u0_tf - self.U0_pred))

        self.optimizer = tf.contrib.opt.ScipyOptimizerInterface(self.loss, 
                                                                method = 'L-BFGS-B', 
                                                                options = {'maxiter': 10000,
                                                                           'maxfun': 50000,
                                                                           'maxcor': 50,
                                                                           'maxls': 50,
                                                                           'ftol' : 1.0 * np.finfo(float).eps})
        
        self.optimizer_Adam = tf.train.AdamOptimizer()
        self.train_op_Adam = self.optimizer_Adam.minimize(self.loss)

        self.saver = tf.train.Saver()

        init = tf.global_variables_initializer()
        self.sess.run(init)


        
    def initialize_NN(self, layers):        
        weights = []
        biases = []
        num_layers = len(layers) 
        for l in range(0,num_layers-1):
            W = self.xavier_init(size=[layers[l], layers[l+1]])
            b = tf.Variable(tf.zeros([1,layers[l+1]], dtype=tf.float32), dtype=tf.float32)
            weights.append(W)
            biases.append(b)        
        return weights, biases
        
    def xavier_init(self, size):
        in_dim = size[0]
        out_dim = size[1]        
        xavier_stddev = np.sqrt(2/(in_dim + out_dim))
        return tf.Variable(tf.truncated_normal([in_dim, out_dim], stddev=xavier_stddev), dtype=tf.float32)
    
    def neural_net(self, X, weights, biases):
        num_layers = len(weights) + 1
        
        # H = 2.0*(X - self.lb)/(self.ub - self.lb) - 1.0
        H = X
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.tanh(tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y

    def net_U0(self, X):
        U1 = self.neural_net(X, self.weights, self.biases)

        q_dot_stages = U1[:,0:self.q]
        q_stages     = X + self.dt*tf.matmul(q_dot_stages, self.IRK_weights[0:self.q, :].T)

        acce         = - 2*self.D*self.omega_0*q_dot_stages - omega_0*omega_0*q_stages

        U0 = U1 - self.dt*tf.matmul(acce, self.IRK_weights.T) # u_t -f = 0

        return U0

    def net_U1(self, X):
        U1   = self.neural_net(X, self.weights, self.biases)
        q_dot_stages = U1[:,:-1]
        q_na         = X + self.dt*tf.matmul(q_dot_stages, self.IRK_weights[self.q:self.q+1, :].T)

        return U1, q_na # N x (q+1)
    
    def callback(self, loss):
        print('Loss:', loss)
    
    def train(self, nIter):
        tf_dict = {self.x0_tf: self.x0, self.u0_tf: self.u0}
        
        start_time = time.time()
        for it in range(nIter):
            self.sess.run(self.train_op_Adam, tf_dict)
            
            # Print
            if it % 10 == 0:
                elapsed = time.time() - start_time
                loss_value = self.sess.run(self.loss, tf_dict)
                print('It: %d, Loss: %.3e, Time: %.2f' %
                      (it, loss_value, elapsed))
                start_time = time.time()

        self.optimizer.minimize(self.sess,
                                feed_dict = tf_dict,
                                fetches = [self.loss], loss_callback = self.callback)
    
    def predict(self, x_star):
        
        U1_star, q_na = self.sess.run([self.U1_pred, self.q_na], {self.x0_tf: x_star})
                    
        return U1_star, q_na

    def save(self):
        self.saver.save(self.sess, 'tmp/1D_vibration')

    def loadModel(self):
        self.saver.restore(self.sess, "tmp/1D_vibration")
    
if __name__ == "__main__": 

    # Train or run
    FLAG='TRAIN'

    # parameter
    k = 1     # spring stiffness
    d = 0.1   # damping coefficient
    m = 1     # mass

    omega_0 = np.sqrt(k/m)
    D = d / 2 * np.sqrt(k * m)
    omega_d = omega_0 * np.sqrt(1 - D * D)
    q_hat = 1.0

    # Analytical solution
    q_sol, t = vibration(omega_0, D, omega_d, q_hat)

    q = 8
    layers = [1, 20, 20, q+1]
    N      = 500

    # start time
    start_time = time.time()

    # time increment, initial condition
    dt = 10.0

    x0 = np.zeros((1, 1))
    u0 = np.zeros((1, 1))

    x0[0,0] = q_sol[0]
    # u0[0,0] = 0.0

    t_nn_steps = 2

    '''Numerical solution'''
    timeAmplitude = np.zeros((t_nn_steps+1, 2))
    timeAmplitude[0, 1] = x0[0, 0] # initial amplitude

    '''Loop over time steps'''
    for t_step in range(1, t_nn_steps+1):


        # build model
        model = PhysicsInformedNN(x0, u0, layers, dt, omega_0, D, omega_d, q)

        # train or load model
        adamSteps = 100
        model.train(adamSteps)

        U1_pred, q_na = model.predict(x0)

        timeAmplitude[t_step, 0] = dt*t_step
        timeAmplitude[t_step, 1] = q_na

        '''Update solution'''
        x0[0, 0] = q_na
        u0[0, 0] = U1_pred[:, -1]

    


    # '''Plot solution'''
    #
    # plt.plot(t, q_sol, label='analytical')
    # plt.plot(timeAmplitude[:, 0], timeAmplitude[:, 1], 'ro', color='red', markersize=5, label='numerical')
    # plt.plot(timeAmplitude[:, 0], timeAmplitude[:, 1], color='red')
    # plt.legend()
    # plt.savefig('figures/1D_vibration/solution.png')
    # plt.close()

    '''Save result'''
    np.save('res_' + str(dt), timeAmplitude)