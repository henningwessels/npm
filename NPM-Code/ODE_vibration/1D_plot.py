
"""
@author: Henning Wessels

"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from matplotlib.ticker import ScalarFormatter
import matplotlib.ticker as mtick
import matplotlib.ticker



class OOMFormatter(matplotlib.ticker.ScalarFormatter):
    def __init__(self, order=0, fformat="%1.1f", offset=True, mathText=True):
        self.oom = order
        self.fformat = fformat
        matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,useMathText=mathText)
    def _set_orderOfMagnitude(self, nothing):
        self.orderOfMagnitude = self.oom
    def _set_format(self, vmin, vmax):
        self.format = self.fformat
        #if self._useMathText:
            #self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)


f, ax = plt.subplots(1, figsize=[10.4, 4.8])

max_t = 25

'''Compute and plot analytical solution'''


def vibration(t):

    # parameter
    k = 1  # spring stiffness
    d = 0.1  # damping coefficient
    m = 1  # mass

    omega_0 = np.sqrt(k / m)
    D = d / 2 * np.sqrt(k * m)
    omega_d = omega_0 * np.sqrt(1 - D * D)
    q_hat = 1.0

    q_sol = q_hat * np.exp(-D * omega_0 * t) * np.cos(omega_d * t)

    return q_sol

# temporal discretization and solution
t    = np.arange(0, max_t, 1e-2)
q_sol = vibration(t)

# plot solution
plt.plot(t, q_sol, label='analytical', linewidth=3, color='black')


#######################################


'''Plot numerical solution'''

dt_all = [10.0]         # for comparison of different time steps, create the folders accordingly and adjust dt_all

form = '*'
iter = 1



for dt in dt_all:



    sloshingAmplitude = np.load('container_' + str(dt) + '/res_' + str(dt) + '.npy')

    plt.plot(sloshingAmplitude[:, 0], sloshingAmplitude[:, 1], form, color='black', markersize=12, label='dt = ' + str(int(dt)) + ' s')

    if iter == 1:
        form = '^'
    elif iter == 2:
        form = 'ro'
    # elif iter == 3:
    #     form = 'burlywood'
    # elif iter == 4:
    #     # color = 'grey'

    iter = iter + 1

plt.grid()

plt.rc('font', **{'family': 'serif', 'serif': ['Times']})
plt.rc('font', size=20)

plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., prop={'size': 20})

plt.xlabel('Time [s]')
plt.ylabel('Amplitude [m]')

plt.gcf().subplots_adjust(bottom=0.2)
plt.gcf().subplots_adjust(left=0.14)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
# ax.yaxis.set_major_formatter(OOMFormatter(-2, "%.1f"))

plt.savefig('compareTimeSteps.png')
plt.close()

