"""
@author: Henning Wessels

Postprocessing: comparison of numerical results with experimental data of Martin and Moyce (1952)

"""

import numpy as np
import matplotlib.pyplot as plt
import math
import sys


from scipy.interpolate import griddata
import matplotlib.ticker as ticker

import scipy.io


testCase = 'damBreak'

T_max = 3
L     = 14.6e-2
g     = 9.8


exp_data = np.array([[1.22, 0.82], [1.44, 1.19], [1.89, 1.58], [2.33, 1.91], [2.78, 2.23], [3.22, 2.58], [3.67, 2.91], [4.11, 3.26]])

# dt_all = [5e-3, 1e-2]
dt_all = [1e-2]

x_min, x_max = 0.0, 3.2
y_min, y_max = 1.0, 4.2

'''Plot'''
f, ax = plt.subplots(1, figsize=[10.4, 4.8])
ax.set_aspect('equal')

plt.rc('font', **{'family': 'serif', 'serif': ['Times']})
plt.rc('font', size=20)

plt.plot(exp_data[:,1], exp_data[:, 0], '*', color='grey', markersize=12, label='experimental')


iter = 0
color = 'black'

for dt in dt_all:

	t_max_step = int(T_max/dt)

	front      = np.zeros(t_max_step + 1)
	time       = np.arange(0, T_max + 1e-10, dt) * np.sqrt(2*g / L)

	for t_step in range(0, t_max_step+1):

		try:
			'''Read data'''
			IC = scipy.io.loadmat('damBreak_twoSets_dt_' + str(dt) + '/IC_BC/' + testCase + '/IC_t' + str(t_step) + '.mat')['IC']

		except:
			front = front[0:t_step-1]
			time  = time[0:t_step-1]

			break


		x = IC[:, 0].flatten()[:, None]
		front[t_step] = np.max(x)/L

	if iter == 1:
		color = 'cadetblue'
	elif iter == 2:
		color = 'olive'
	elif iter == 3:
		color = 'burlywood'
	elif iter == 4:
		color = 'grey'

	iter = iter+1


	plt.plot(time, front, linewidth=3, color=color, label='dt = ' + str(dt) + ' s')
# plt.plot(time, front, 'ro', color='grey', markersize=12)



ax.tick_params(axis='both', labelsize=20)
major_ticks_x = np.arange(x_min, x_max + 1e-10, 1.0)
major_ticks_y = np.arange(y_min, y_max + 1e-10, 1.0)
ax.set_xticks(major_ticks_x)
ax.set_yticks(major_ticks_y)

minor_ticks_x = np.arange(x_min, x_max + 1e-10, 0.1)
minor_ticks_y = np.arange(y_min, y_max + 1e-10, 0.1)
ax.set_xticks(minor_ticks_x, minor=True)
ax.set_yticks(minor_ticks_y, minor=True)

ax.set_xlim(x_min, x_max)
ax.set_ylim(y_min, y_max)

plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., prop={'size': 20})


plt.xlabel(r'$T = t \sqrt{2g/L}$', fontsize=20)
plt.ylabel(r'$X = x/L$', fontsize=20)

plt.gcf().subplots_adjust(bottom=0.2)
plt.gcf().subplots_adjust(left=0.14)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])

plt.grid()

plt.savefig('experimental.png')
plt.close()

