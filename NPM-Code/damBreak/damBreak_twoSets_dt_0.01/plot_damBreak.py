"""
@author: Henning Wessels

Postprocessing: Plot dam break particle distribution

"""

import numpy as np
import matplotlib.pyplot as plt
import math
import sys


from scipy.interpolate import griddata
import matplotlib.ticker as ticker

import scipy.io


testCase = 'damBreak'
t_max_step  = 30
start = 0

L = 14.6e-2

x_min, x_max, y_min, y_max = 0.0, 4*L, 0.0, 2.5*L

try: 

    for t_step in range(start, t_max_step):
        IC = scipy.io.loadmat('IC_BC/' + testCase + '/IC_t' + str(t_step) + '.mat')['IC']


        x = IC[:, 0].flatten()[:, None]
        y = IC[:, 1].flatten()[:, None]
        pressure = IC[:, 4]


        '''Plot'''
        f, ax = plt.subplots(1)
        ax.set_aspect('equal')

        plt.scatter(x, y, color='grey', s=3)

        ax.tick_params(axis='both', labelsize=20)
        major_ticks_x = np.arange(x_min, x_max + 1e-10, L)
        major_ticks_y = np.arange(y_min, y_max + 1e-10, L)
        ax.set_xticks(major_ticks_x)
        ax.set_yticks(major_ticks_y)

        ax.set_xlim(x_min*0.9, x_max*1.1)
        ax.set_ylim(y_min*0.9, y_max*1.1)

        plt.xlabel(r'$x$', fontsize=20)
        plt.ylabel(r'$y$', fontsize=20)

        plt.gcf().subplots_adjust(bottom=0.2)
        plt.gcf().subplots_adjust(left=0.2)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 1.0, box.height])
        plt.grid()

        plt.savefig('figures/' + testCase + '/video/grid_t'+ str(t_step) + '.png')
        plt.close()


except:
    pass

