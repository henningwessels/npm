"""
@author: Henning Wessels

This code is based on the work of Maziar Raissi.
For execution, the folder 'Utilities' must be downloaded from
https://github.com/maziarraissi/PINNs/

"""

import sys
sys.path.insert(0, '/home/wessels/Utilities/')
# sys.path.insert(0, '/global/')



import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import time
import scipy.io
from plotting import newfig, savefig
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import multiprocessing

# private functions
import initialization as init



np.random.seed(1234)
tf.compat.v1.set_random_seed(1234)

def updateWall(x, y, id_u1_x, id_u1_y, u0):

    tol   = 1e-3

    for i in range(0, len(id_u1_x)):
        id = id_u1_x[i]

        if (y[id]<tol) & (u0[id, 1]<=0):

            print('shift X-boun (%.5f, %.5f) ' % (x[id], y[id]))
            if x[id]==0:
                y[id] = 0
                x[id] = tol

                u0[id, 0] = -u0[id, 1]
                u0[id, 1] = 0

            print('after shift: (%.5f, %.5f)' % (x[id], y[id]))

            '''switch id'''
            id_u1_y = np.append(id_u1_y, id)
            id_u1_x = np.delete(id_u1_x, i)
            break



    for i in range(0, len(id_u1_y)):
        id = id_u1_y[i]

        if (x[id]<tol) & (u0[id, 0]<=0):

            print('shift Y-boun (%.5f, %.5f)' % (x[id], y[id]))

            x[id] = 0
            y[id] = tol

            u0[id, 1] = -u0[id, 0]
            u0[id, 0] = 0

            print('after shift: (%.5f, %.5f)' % (x[id], y[id]))

            '''switch id'''
            id_u1_x = np.append(id_u1_x, id)
            id_u1_y = np.delete(id_u1_y, i)
            break



    return x, y, id_u1_x, id_u1_y, u0


def plot(x, y, U_plot, filename, testCase, t_step):

    '''Generic formatting'''

    cbformat = ticker.ScalarFormatter()  # create the formatter
    cbformat.set_powerlimits((-2, 2))


    f, ax = plt.subplots(1)

    plt.scatter(x, y, c=U_plot, cmap='RdBu_r')
    plt.xlabel(r'$x$', fontsize=20)
    plt.ylabel(r'$y$', fontsize=20)

    cb = plt.colorbar(format=cbformat)

    if(filename=='pressure_t'):
        cb.set_label(label=r'$p$', fontsize=20)
    elif(filename=='x-velocity_t'):
        cb.set_label(label=r'$v_x$', fontsize=20)
    elif(filename=='y-velocity_t'):
        cb.set_label(label=r'$v_y$', fontsize=20)
    elif(filename == 'velocity_magnitude_t'):
        cb.set_label(label=r'$\|\mathbf{v}\|$', fontsize=20)
    elif(filename=='divergence_t'):
        cb.set_label(label=r'$\|\mbox{div}\,\mathbf{v}\|$', fontsize=20)
    elif(filename=='x-dist-net_t'):
        cb.set_label(label=r'$D_x$', fontsize=20)
    elif (filename == 'y-dist-net_t'):
        cb.set_label(label=r'$D_y$', fontsize=20)

    for l in cb.ax.yaxis.get_ticklabels():
        # l.set_weight("bold")
        l.set_fontsize(20)

    ax.tick_params(axis='both', labelsize=20)
    major_ticks = np.arange(lb[0], ub[0] + 1e-10, 0.5)
    ax.set_xticks(major_ticks)
    ax.set_yticks(major_ticks)

    plt.gcf().subplots_adjust(bottom=0.2)
    # plt.gcf().subplots_adjust(left=0.0)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 1.0, box.height])

    plt.savefig('figures/' + testCase + '/' + filename + str(t_step) + '.png')
    plt.close()


class PhysicsInformedNN:
    # Initialize the class
    def __init__(self, id_same, idu1_p, x0, y0, rho0, u0, layers, dt, lb, ub, q, mu, testCase, t_step):

        'LOAD DATA'''

        self.testCase = testCase
        self.t_step = t_step

        # fixed and free node indices
        self.idu1_p = idu1_p

        # material data
        self.rho0  = rho0
        self.mu    = mu

        # gravity acceleration
        self.b_x = 0.0
        self.b_y = -9.8

        # lower and upper bound
        self.lb = lb
        self.ub = ub

        # initial coordinates and solution
        self.x0 = x0[id_same].flatten()[:, None]     # initial x-position
        self.y0 = y0[id_same].flatten()[:, None]     # initial y-position

        # domain height and width for distance function
        self.width  = np.max(x0) - np.min(x0)
        self.height = np.max(y0) - np.min(y0)

        print('width = %.3e'  % (self.width))
        print('height = %.3e' % (self.height))

        self.u0_x = u0[:, 0].flatten()[:, None]    # initial x-velocity
        self.u0_y = u0[:, 1].flatten()[:, None]    # initial y-velocity

        # boundary pressure
        self.x1_p = x0[idu1_p].flatten()[:, None]          # boundary x-position
        self.y1_p = y0[idu1_p].flatten()[:, None]          # boundary y-position
        # self.u1_p = u0[idu1_p, 2:3].flatten()[:, None]     # boundary solution

        # others
        self.layers = layers
        self.dt = dt
        self.q = max(q,1)
    
        # Initialize NN
        self.weights, self.biases = self.initialize_NN(layers)
        
        # Load IRK weights
        tmp = np.float32(np.loadtxt('/home/wessels/Utilities/IRK_weights/Butcher_IRK%d.txt' % (q), ndmin = 2))
        self.IRK_weights = np.reshape(tmp[0:q**2+q], (q+1,q))
        self.IRK_times = tmp[q**2+q:]

        # graph
        self.sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(allow_soft_placement=True))
                                                     # , log_device_placement=True))

        'PLACEHOLDERS'

        # placeholder for initial position
        self.x0_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.x0.shape[1]), name='x0_tf')
        self.y0_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.y0.shape[1]), name='y0_tf')

        # placeholder for initial density
        self.rho0_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.rho0.shape[1]), name='rho0_tf')

        # placeholder for boundary coordinates
        self.x1_p_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.x1_p.shape[1]), name='x1_p_tf')
        self.y1_p_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.y1_p.shape[1]), name='y1_p_tf')

        # placeholder for concatenated boundary coordinates
        self.x1_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.x0.shape[1]), name='x1_tf')
        self.y1_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.y0.shape[1]), name='y1_tf')

        # placeholder for initial condition
        self.u0_x_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.u0_x.shape[1]), name='u0_x_tf')
        self.u0_y_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.u0_y.shape[1]), name='u0_y_tf')

        # dummy variables for velocity gradients
        self.dummy_x0_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.q), name='dummy_x0_tf') # dummy variable for fwd_gradients
        self.dummy_x0_p_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, 1), name='dummy_x0_p_tf') # dummy variable for pressure gradient
        self.dummy_x1_tf = tf.compat.v1.placeholder(tf.float32, shape=(None, self.q+1), name='dummy_x1_tf') # dummy variable for fwd_gradients


        self.U0_x_pred, self.U0_y_pred, self.divU = self.balance_equations(self.x0_tf, self.y0_tf, self.rho0_tf) # N x 2*(q+1)+1
        self.U1_x_out, self.U1_y_out, self.divU_out, self.detF_out, self.x_out, self.y_out, self.p_out = self.output_solution(self.x1_tf, self.y1_tf)          # N1 x 2*(q+1)+1
        self.U1_p_pred = self.net_U1_p(self.x1_p_tf, self.y1_p_tf)

        '''Initial pressure not penalized!!!'''

        # self.weightBC = 1e3
        self.loss = tf.reduce_sum(tf.square(self.U1_p_pred)) \
                     + tf.reduce_sum(tf.square(self.u0_x_tf - self.U0_x_pred)) \
                     + tf.reduce_sum(tf.square(self.u0_y_tf - self.U0_y_pred)) \
                     + tf.reduce_sum(tf.square(self.divU))

        self.optimizer = tf.contrib.opt.ScipyOptimizerInterface(self.loss,
                                                                method = 'L-BFGS-B',
                                                                options = {'maxiter': 1000000,
                                                                           'maxfun': 50000,
                                                                           'maxcor': 50,
                                                                           'maxls': 50,
                                                                           'ftol' : 1.0 * np.finfo(float).eps})

        self.optimizer_Adam = tf.compat.v1.train.AdamOptimizer(learning_rate=0.0001)
        self.train_op_Adam = self.optimizer_Adam.minimize(self.loss)

        self.saver = tf.compat.v1.train.Saver()

        init = tf.compat.v1.global_variables_initializer()
        self.sess.run(init)


        
    def initialize_NN(self, layers):        
        weights = []
        biases = []
        num_layers = len(layers) 
        for l in range(0,num_layers-1):
            W = self.xavier_init(size=[layers[l], layers[l+1]])
            b = tf.Variable(tf.zeros([1,layers[l+1]], dtype=tf.float32), dtype=tf.float32)
            weights.append(W)
            biases.append(b)        
        return weights, biases
        
    def xavier_init(self, size):
        in_dim = size[0]
        out_dim = size[1]        
        xavier_stddev = np.sqrt(2/(in_dim + out_dim))
        return tf.Variable(tf.random.truncated_normal([in_dim, out_dim], stddev=xavier_stddev), dtype=tf.float32)
    
    def neural_net(self, X, weights, biases):
        num_layers = len(weights) + 1
        
        H = 2.0*(X - self.lb)/(self.ub - self.lb) - 1.0
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.tanh(tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y
    
    def fwd_gradients_x0(self, U, x):
        g = tf.gradients(U, x, grad_ys=self.dummy_x0_tf)[0]
        return tf.gradients(g, self.dummy_x0_tf)[0]

    def fwd_gradients_y0(self, U, y):
        g = tf.gradients(U, y, grad_ys=self.dummy_y0_tf)[0]
        return tf.gradients(g, self.dummy_y0_tf)[0]

    def fwd_gradients_x0_p(self, U, x):
        g = tf.gradients(U, x, grad_ys=self.dummy_x0_p_tf)[0]
        return tf.gradients(g, self.dummy_x0_p_tf)[0]

    def fwd_gradients_y0_p(self, U, y):
        g = tf.gradients(U, y, grad_ys=self.dummy_y0_p_tf)[0]
        return tf.gradients(g, self.dummy_y0_p_tf)[0]


    def fwd_gradients_x1(self, U, x):
        g = tf.gradients(U, x, grad_ys=self.dummy_x1_tf)[0]
        return tf.gradients(g, self.dummy_x1_tf)[0]

    def fwd_gradients_y1(self, U, y):
        g = tf.gradients(U, y, grad_ys=self.dummy_y1_tf)[0]
        return tf.gradients(g, self.dummy_y1_tf)[0]

    def balance_equations(self, X, Y, RHO_0):

        ''''''
        '''Distance function and boundary extension'''
        D_x   = X/self.width
        D_x_x = 1/self.width
        D_x_y = 0

        D_y    = Y/self.height
        D_y_x  = 0
        D_y_y  = 1/self.height

        '''Neural network'''
        U1 = self.neural_net(tf.concat([X,Y], 1), self.weights, self.biases)

        '''RK velocity stages & solution'''

        U1_hat_x = U1[:, 0:self.q+1]
        U1_hat_y = U1[:, (self.q+1):(2*self.q+2)]

        U1_x  = D_x*U1[:, 0:(self.q+1)]
        U1_y  = D_y*U1[:, (self.q+1):(2*self.q + 2)]

        '''Pressure RK stages'''
        p_stages   = U1[:, (2*self.q+2):(3*self.q+2)]      # RK pressure stages

        '''Grad v'''
        dot_F_11 = D_x*self.fwd_gradients_x1(U1_hat_x, X) + D_x_x*U1_hat_x
        dot_F_12 = D_x*self.fwd_gradients_x1(U1_hat_x, Y) + D_x_y*U1_hat_x

        dot_F_21 = D_y*self.fwd_gradients_x1(U1_hat_y, X) + D_y_x*U1_hat_y
        dot_F_22 = D_y*self.fwd_gradients_x1(U1_hat_y, Y) + D_y_y*U1_hat_y

        '''Delta F'''
        F_11 = self.dt*tf.matmul(dot_F_11[:, 0:self.q], self.IRK_weights.T) + 1
        F_12 = self.dt*tf.matmul(dot_F_12[:, 0:self.q], self.IRK_weights.T)

        F_21 = self.dt*tf.matmul(dot_F_21[:, 0:self.q], self.IRK_weights.T)
        F_22 = self.dt*tf.matmul(dot_F_22[:, 0:self.q], self.IRK_weights.T) + 1

        '''Inverse of deformation gradient'''
        detF = tf.multiply(F_11, F_22) - tf.multiply(F_12, F_21)

        F_inv_11 =  tf.divide(F_22, detF)
        F_inv_12 = -tf.divide(F_12, detF)

        F_inv_21 = -tf.divide(F_21, detF)
        F_inv_22 =  tf.divide(F_11, detF)

        '''!!! Momentum equation: only stages go into acceleration computation!!'''

        '''Density inverse'''
        invRho = tf.divide(1.0, RHO_0)

        '''Grad p'''
        Grad_p_X = self.fwd_gradients_x0(-p_stages, X)
        Grad_p_Y = self.fwd_gradients_x0(-p_stages, Y)

        grad_p_x = tf.multiply(F_inv_11[:, 0:self.q], Grad_p_X) + tf.multiply(F_inv_21[:, 0:self.q], Grad_p_Y)
        grad_p_y = tf.multiply(F_inv_12[:, 0:self.q], Grad_p_X) + tf.multiply(F_inv_22[:, 0:self.q], Grad_p_Y)

        '''Contact acceleration'''

        x_stages = X + self.dt*tf.matmul(U1_x[:, 0:self.q], self.IRK_weights[0:self.q, :].T)
        y_stages = Y + self.dt*tf.matmul(U1_y[:, 0:self.q], self.IRK_weights[0:self.q, :].T)
        eps = 1e7

        # left boundary
        normal_left  = -1.0
        gap_left     = (x_stages - 0.0)*normal_left
        active_left  = 0.5*(1.0 + tf.sign(gap_left))
        penalty_left = eps*gap_left*active_left*normal_left

        # bottom boundary
        normal_bottom  = -1.0
        gap_bottom     = (y_stages - 0.0)*normal_bottom
        active_bottom  = 0.5*(1.0 + tf.sign(gap_bottom))
        penalty_bottom = eps*gap_bottom*active_bottom*normal_bottom

        '''Momentum equation in x and y'''
        acce_x = self.b_x + tf.multiply(invRho, grad_p_x) - penalty_left #- penalty_right
        acce_y = self.b_y + tf.multiply(invRho, grad_p_y) - penalty_bottom


        '''Time integration''' # v_n_i = v_n+1 - dt*sum_i a_ji f(xi_i) \\ v_n_q+1 = v_n+1 - dt*sum_j b_j f(xi_j)
        U0_x = U1_x - self.dt*tf.matmul(acce_x, self.IRK_weights.T)
        U0_y = U1_y - self.dt*tf.matmul(acce_y, self.IRK_weights.T)


        '''!!! Mass balance'''

        '''Spatial velocity gradient l'''
        l_11 = tf.multiply(dot_F_11, F_inv_11) + tf.multiply(dot_F_12, F_inv_21)
        l_22 = tf.multiply(dot_F_21, F_inv_12) + tf.multiply(dot_F_22, F_inv_22)

        divU = l_11 + l_22


        return U0_x, U0_y, divU

    def output_solution(self, X, Y):

        ''''''
        '''Distance function and boundary extension'''
        D_x   = X/self.width
        D_x_x = 1/self.width
        D_x_y = 0

        D_y    = Y/self.height
        D_y_x  = 0
        D_y_y  = 1/self.height

        '''Neural network'''
        U1 = self.neural_net(tf.concat([X ,Y], 1), self.weights, self.biases)

        '''RK velocity stages & solution'''
        U1_hat_x = U1[:, 0:(self.q + 1)]
        U1_hat_y = U1[:, (self.q + 1):(2 * self.q + 2)]

        U1_x  = D_x*U1_hat_x
        U1_y  = D_y*U1_hat_y

        '''Pressure RK stages'''
        p_stages = U1[:, (2*self.q+2):(3*self.q+2)]      # RK pressure stages

        '''Updated Position and pressure'''
        b_j  = self.IRK_weights[self.q:(self.q+1),:].T
        x    = X + self.dt*tf.matmul(U1_x[:, 0:self.q], b_j)
        y    = Y + self.dt*tf.matmul(U1_y[:, 0:self.q], b_j)
        p_na = tf.matmul(p_stages, b_j)

        '''Grad v'''
        dot_F_11 = D_x*self.fwd_gradients_x1(U1_hat_x, X) + D_x_x*U1_hat_x
        dot_F_12 = D_x*self.fwd_gradients_x1(U1_hat_x, Y) + D_x_y*U1_hat_x

        dot_F_21 = D_y*self.fwd_gradients_x1(U1_hat_y, X) + D_y_x*U1_hat_y
        dot_F_22 = D_y*self.fwd_gradients_x1(U1_hat_y, Y) + D_y_y*U1_hat_y


        '''Delta F'''
        F_11 = self.dt*tf.matmul(dot_F_11[:, 0:self.q], b_j) + 1
        F_12 = self.dt*tf.matmul(dot_F_12[:, 0:self.q], b_j)

        F_21 = self.dt*tf.matmul(dot_F_21[:, 0:self.q], b_j)
        F_22 = self.dt*tf.matmul(dot_F_22[:, 0:self.q], b_j) + 1

        '''Inverse of deformation gradient'''
        detF = tf.multiply(F_11, F_22) - tf.multiply(F_12, F_21)

        F_inv_11 =  tf.divide(F_22, detF)
        F_inv_12 = -tf.divide(F_12, detF)

        F_inv_21 = -tf.divide(F_21, detF)
        F_inv_22 =  tf.divide(F_11, detF)

        '''Spatial velocity gradient l'''
        l_11 = tf.multiply(dot_F_11[:, self.q:(self.q+1)], F_inv_11) + tf.multiply(dot_F_12[:, self.q:(self.q+1)], F_inv_21)
        l_22 = tf.multiply(dot_F_21[:, self.q:(self.q+1)], F_inv_12) + tf.multiply(dot_F_22[:, self.q:(self.q+1)], F_inv_22)

        '''div v = tr l'''

        divU = l_11 + l_22

        return U1_x, U1_y, divU, detF, x, y, p_na # N x (2*(q+1) +1)


    def net_U1_p(self, x1_p, y1_p):

        U1 = self.neural_net(tf.concat([x1_p, y1_p], 1), self.weights, self.biases)
        U1_p = U1[:, 2*(self.q+1):3*(self.q+1)]

        return U1_p # N x (2*(q+1) +1)

    def callback(self, loss):
        print('Loss:', loss)
    
    def train(self, nIter):
        tf_dict = {self.x0_tf: self.x0, self.y0_tf: self.y0,
                   self.u0_x_tf: self.u0_x, self.u0_y_tf: self.u0_y,
                   self.rho0_tf: self.rho0,
                   self.x1_p_tf: self.x1_p, self.y1_p_tf: self.y1_p,
                   self.x1_tf: np.zeros((self.x0.shape[0], 1)),
                   self.y1_tf: np.zeros((self.y0.shape[0], 1)),
                   self.dummy_x0_tf: np.ones((self.x0.shape[0], self.q)),
                   self.dummy_x0_p_tf: np.ones((self.x0.shape[0], 1)),
                   self.dummy_x1_tf: np.ones((self.x0.shape[0], self.q+1)),}
        
        start_time = time.time()
        for it in range(nIter):
            self.sess.run(self.train_op_Adam, tf_dict)
            
            # Print
            if it % 1000 == 0:
                elapsed = time.time() - start_time
                loss_value = self.sess.run(self.loss, tf_dict)
                print('It: %d, Loss: %.3e, Time: %.2f' %
                      (it, loss_value, elapsed))
                start_time = time.time()

        self.optimizer.minimize(self.sess,
                                feed_dict = tf_dict,
                                fetches = [self.loss])

    
    def predict(self, x_star, y_star):

        U1_x, U1_y, divU_out, detF_out, x_out, y_out, p_out = self.sess.run([self.U1_x_out, self.U1_y_out, self.divU_out, self.detF_out, self.x_out, self.y_out, self.p_out], {self.x1_tf: x_star, self.y1_tf: y_star,
                                                                                                                                                            self.dummy_x0_p_tf: np.ones((x_star.shape[0], 1)),
                                                                                                                                                            self.dummy_x1_tf: np.ones((x_star.shape[0], self.q+1))})
                    
        return U1_x, U1_y, divU_out, detF_out, x_out, y_out, p_out

    def save(self):
        self.saver.save(self.sess, 'tmp/' + str(self.t_step) + '/' + self.testCase)

    def loadModel(self):
        self.saver.restore(self.sess, 'tmp/' + str(self.t_step) + '/' + self.testCase)



def run_tensorflow():

    testCase    = 'damBreak'

    '''Network layout'''
    q = 20
    layers = [2, 60, 60, (3*(q+1)-1)]

    '''Read time step from file'''
    TempDisc   = scipy.io.loadmat('IC_BC/' + testCase + '/timeStep.mat')
    dt         = TempDisc['dt'][0,0]
    idt        = TempDisc['idt'][0]
    t_step     = int(idt[0])
    idt        = np.delete(idt, 0)
    scipy.io.savemat('IC_BC/' + testCase + '/timeStep.mat', {'dt': dt, 'idt': idt})

    print('tstep: %d' % t_step)

    '''Read model parameter from file'''
    IC = scipy.io.loadmat('IC_BC/' + testCase + '/IC_t' + str(t_step-1) + '.mat')['IC']
    BC = scipy.io.loadmat('IC_BC/' + testCase + '/BC_t' + str(t_step-1) + '.mat')

    '''Initial conditions'''
    x    = IC[:, 0].flatten()[:, None]
    y    = IC[:, 1].flatten()[:, None]
    u0   = IC[:, 2:5]
    rho0 = IC[:, 5].flatten()[:, None]

    mu   = 0.01    # dynamic viscosity

    '''Boundary conditions'''
    id_u1_x   = BC['id_u1_x'][0]
    id_u1_y   = BC['id_u1_y'][0]
    id_u1_p   = BC['id_u1_p']
    id_free_p = BC['id_free_p']
    id_free_x = BC['id_free_x'][0]
    id_free_y = BC['id_free_y'][0]
    id_same   = BC['id_same'][0]

    sim_id_u1_p = np.intersect1d(id_same, id_u1_p)


    '''Lower and upper bounds'''
    lb = np.array([np.min(x), np.min(y)])
    ub = np.array([np.max(x), np.max(y)])


    '''build model'''
    start_time = time.time()
    model = PhysicsInformedNN(id_same, sim_id_u1_p, x, y, rho0[id_same], u0[id_same, :], layers, dt, lb, ub, q, mu, testCase, t_step)

    # train or load model
    adamSteps = 12000
    model.train(adamSteps)

    # predict
    U1_x, U1_y, divU_out, detF, x_out, y_out, P_plot = model.predict(x, y)

    # error in incompressibility
    error_incomp = np.linalg.norm(divU_out, 2)

    # print elapsed time
    elapsed_time = time.time() - start_time
    print('Error: %e \t Total elapsed: %e' % (error_incomp, elapsed_time))

    '''Update solution'''
    U_x_plot   = np.zeros((len(y), 1))
    U_y_plot   = np.zeros((len(y), 1))

    U_x_plot[:, 0] = np.transpose(U1_x[:, q])
    U_y_plot[:, 0] = np.transpose(U1_y[:, q])

    u0[id_free_x, 0] = U_x_plot[id_free_x, 0]
    u0[id_free_y, 1] = U_y_plot[id_free_y, 0]
    u0[:, 2] = P_plot[:, 0]

    '''Update coordinates (x,y) and density'''
    x[id_free_x] = x_out[id_free_x]
    y[id_free_y] = y_out[id_free_y]

    '''Update index list'''
    x, y, id_u1_x, id_u1_y, u0 = updateWall(x, y, id_u1_x, id_u1_y, u0)

    for iter in range(0, len(id_u1_p)):
        if(x[iter]<1e-3):
            id_u1_p = np.delete(id_u1_p, iter)



    id_free_x = np.arange(0, len(x), 1)
    id_free_x = np.delete(id_free_x, id_u1_x)

    id_free_y = np.arange(0, len(y), 1)
    id_free_y = np.delete(id_free_y, id_u1_y)

    '''Plot field'''
    U_mag_plot = np.sqrt(np.square(U_x_plot) + np.square(U_y_plot))

    plot(x, y, P_plot, 'pressure_t', testCase, t_step)
    plot(x, y, U_x_plot, 'x-velocity_t', testCase, t_step)
    plot(x, y, U_y_plot, 'y-velocity_t', testCase, t_step)
    plot(x, y, U_mag_plot, 'y-velocity_t', testCase, t_step)
    plot(x, y, divU_out, 'divergence_t', testCase, t_step)
    plot(x[id_u1_p], y[id_u1_p], P_plot[id_u1_p], 'p-boun_t', testCase, t_step)

    plot(x[id_u1_x], y[id_u1_x], U_y_plot[id_u1_x], 'x-boun_t', testCase, t_step)
    plot(x[id_u1_y], y[id_u1_y], U_x_plot[id_u1_y], 'y-boun_t', testCase, t_step)


    '''Save result to file'''
    IC = np.concatenate((x, y, u0, rho0, P_plot), axis=1)
    scipy.io.savemat('IC_BC/' + testCase + '/IC_t' + str(t_step) + '.mat', {'IC': IC})

    scipy.io.savemat('IC_BC/' + testCase + '/BC_t' + str(t_step) + '.mat', {'id_same': id_same, 'id_u1_x': id_u1_x, 'id_u1_y': id_u1_y, 'id_u1_p': id_u1_p, 'id_free_x': id_free_x, 'id_free_y': id_free_y, 'id_free_p': id_free_p})






if __name__ == "__main__":

    testCase    = 'damBreak'

    # time increment and number of steps
    dt = 1e-2
    t_nn_steps = 30
    t_start = 0
    idt = np.arange(t_start+1,t_start+t_nn_steps+2,1, dtype=np.float32)
    scipy.io.savemat('IC_BC/' + testCase + '/timeStep.mat', {'dt': dt, 'idt': idt})


    # Initial and boundary conditions
    id_same, id_free_x, id_u1_x, id_free_y, id_u1_y, id_free_p, id_u1_p, x, y, Exact = init.damBreak_twoSets(testCase)
    u0 = Exact.T

    lb = np.array([np.min(x), np.min(y)])
    ub = np.array([np.max(x), np.max(y)])

    # material parameter
    rho0 = np.full(len(x), 997).flatten()[:, None]      # density

    '''Save initial data to file'''
    IC = np.concatenate((x, y, u0, rho0, rho0), axis=1)

    scipy.io.savemat('IC_BC/' + testCase + '/IC_t0.mat', {'IC': IC})
    scipy.io.savemat('IC_BC/' + testCase + '/BC_t0.mat', {'id_same': id_same, 'id_u1_x': id_u1_x, 'id_u1_y': id_u1_y, 'id_u1_p': id_u1_p, 'id_free_x': id_free_x, 'id_free_y': id_free_y, 'id_free_p': id_free_p})

    '''plot initial data'''
    if(t_start==0):

        U_x_plot = np.zeros((len(y), 1))
        U_y_plot = np.zeros((len(y), 1))
        P_plot   = np.ones((len(y), 1))

        U_mag_plot = np.sqrt(np.square(U_x_plot) + np.square(U_y_plot))

        # plot
        plot(x[id_same], y[id_same], P_plot[id_same], 'pressure_t', testCase, 0)
        plot(x[id_same], y[id_same], U_x_plot[id_same], 'x-velocity_t', testCase, 0)
        plot(x[id_same], y[id_same], U_y_plot[id_same], 'y-velocity_t', testCase, 0)
        plot(x[id_same], y[id_same], U_mag_plot[id_same], 'velocity_magnitude_t', testCase, 0)
        plot(x[id_same], y[id_same], P_plot[id_same], 'divergence_t', testCase, 0)



    '''Loop over time steps'''
    for t_step in range(1, t_nn_steps+1):

        p = multiprocessing.Process(target=run_tensorflow)
        p.start()
        p.join()
