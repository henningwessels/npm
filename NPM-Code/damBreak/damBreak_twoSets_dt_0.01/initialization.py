"""
@author: Henning Wessels

"""

import numpy as np
import matplotlib.pyplot as plt
import math
import random




def plotInitialCondition(x, y, u0, idu1, testCase, BC_type):

    # U_plot = np.zeros((len(y), 1))
    # for i in idu1:
    #     U_plot[i, 0] = 1

    f, ax = plt.subplots(1)

    plt.scatter(x[idu1], y[idu1], c=u0[idu1], cmap='RdBu_r')
    cb = plt.colorbar()
    for l in cb.ax.yaxis.get_ticklabels():
        # l.set_weight("bold")
        l.set_fontsize(20)

    ax.tick_params(axis='both', labelsize=20)
    # major_ticks = np.arange(lb[0,0], ub[0,0] + 1e-10, 0.5)
    # ax.set_xticks(major_ticks)
    # ax.set_yticks(major_ticks)

    # plt.show()

    plt.savefig('figures/' + testCase + '/' + BC_type + '.png')
    plt.close()


def distanceFunction(x, y, idu1, testCase, lb, ub, coordinate, t_step):

    '''Boundary and all points'''
    boundary_coll = np.concatenate([x[idu1], y[idu1]], axis=1)
    coll          = np.concatenate([x, y], axis=1)

    '''knn search'''
    neigh = NearestNeighbors(n_neighbors=1)
    neigh.fit(boundary_coll)
    dist = neigh.kneighbors(coll)[0]
    dist = dist/np.max(dist)

    '''Plot distance function'''
    f, ax = plt.subplots(1)
    plt.scatter(x, y, c=dist, cmap='RdBu_r')
    cb = plt.colorbar()
    for l in cb.ax.yaxis.get_ticklabels():
        # l.set_weight("bold")
        l.set_fontsize(20)

    ax.tick_params(axis='both', labelsize=20)
    major_ticks = np.arange(lb[0], ub[0] + 1e-10, 0.5)
    ax.set_xticks(major_ticks)
    ax.set_yticks(major_ticks)

    plt.show()

    plt.savefig('figures/' + testCase + '/distance_' + coordinate + 't_' + str(t_step) + '.png')
    plt.close()

    return dist



def refine(x_coarse, nx):

    x_fine = np.empty(0)
    for i in range(0, len(x_coarse) - 1):
        tmp = np.linspace(x_coarse[i], x_coarse[i + 1], nx)
        x_fine = np.append(x_fine, tmp)

    x_fine = np.unique(x_fine)

    return x_fine


def createList(x, y, x_min, x_max, y_min, y_max, testCase):

    '''Remove edge'''
    id_u1_x = np.where(x == x_min)[0]
    id_u1_y = np.where(y == y_min)[0]

    id_common = np.intersect1d(id_u1_x, id_u1_y)[0]
    x = np.delete(x, id_common)
    y = np.delete(y, id_common)

    '''Velocity boundary'''
    id_u1_x = np.where(x == x_min)[0]
    id_u1_y = np.where(y == y_min)[0]

    '''Pressure boundary at free surface'''
    id_u1_p_x = np.where(y == y_max)[0]
    id_u1_p_y = np.where(x == x_max)[0]

    common_value = np.intersect1d(id_u1_p_x, id_u1_p_y)
    id_common = np.where(id_u1_p_x==common_value)[0]
    id_u1_p_x = np.delete(id_u1_p_x, id_common)
    id_u1_p = np.concatenate([id_u1_p_x, id_u1_p_y], axis=0)

    id_u1_p = id_u1_p.T


    '''Flatten position vector'''
    x.flatten()[:, None]
    y.flatten()[:, None]

    '''Free indices'''
    id_free_x = np.arange(0, len(x), 1)
    id_free_x = np.delete(id_free_x, id_u1_x)

    id_free_y = np.arange(0, len(x), 1)
    id_free_y = np.delete(id_free_y, id_u1_y)

    id_free_p = np.arange(0, len(x), 1)
    id_free_p = np.delete(id_free_p, id_u1_p)

    '''Initial values'''
    T = np.zeros((3, len(x)))

    '''plot initial condition'''
    plotInitialCondition(x, y, T[0 ,:], id_u1_x, testCase, 'x-boun')
    plotInitialCondition(x, y, T[1 ,:], id_u1_y, testCase, 'y-boun')
    plotInitialCondition(x, y, T[2 ,:], id_u1_p, testCase, 'p-boun')

    return x, y, id_u1_x, id_free_x, id_u1_y, id_free_y, id_u1_p, id_free_p, T


def damBreak_twoSets(testCase):


    L       = 14.6e-2
    n_per_L = 20     # points per length unit L

    # fluid discretization
    x_min, x_max = 0, 1*L
    y_min, y_max = 0, 2*L

    '''(x, y) simulation'''
    nx, ny = (n_per_L, 2*n_per_L)
    x_tmp = np.linspace(x_min, x_max, nx)
    y_tmp = np.linspace(y_min, y_max, ny)
    x_sim, y_sim = np.meshgrid(x_tmp, y_tmp)

    x_sim = x_sim.flatten()[:, None]
    y_sim = y_sim.flatten()[:, None]

    '''(x, y) alphashape'''
    x_tmp_2 = refine(x_tmp, 10)
    y_tmp_2 = refine(y_tmp, 10)
    x_alpha, y_alpha = np.meshgrid(x_tmp_2, y_tmp_2)

    '''Create index lists'''
    x_sim, y_sim = createList(x_sim, y_sim, x_min, x_max, y_min, y_max, testCase)[0:2]
    x_alpha, y_alpha, id_u1_x, id_free_x, id_u1_y, id_free_y, id_u1_p, id_free_p, T = createList(x_alpha, y_alpha, x_min, x_max, y_min, y_max, testCase)

    x_alpha = x_alpha.flatten()[:, None]
    y_alpha = y_alpha.flatten()[:, None]

    x_sim   = x_sim.flatten()[:, None]
    y_sim   = y_sim.flatten()[:, None]


    '''get indices of training subset'''

    common_x  = np.intersect1d(x_alpha, x_sim)
    id_same_x = np.unique(np.where(x_alpha == common_x)[0])

    common_y  = np.intersect1d(y_alpha, y_sim)
    id_same_y = np.unique(np.where(y_alpha == common_y)[0])

    id_same = np.intersect1d(id_same_x, id_same_y)

    plt.scatter(x_alpha[id_same], y_alpha[id_same])
    plt.savefig('figures/' + testCase + '/subset.png')

    '''Get distance function'''
    # dist_x = distanceFunction(x, y, idu1_x, testCase, lb, ub, 'x', 0)
    # dist_y = distanceFunction(x, y ,idu1_y, testCase, lb, ub, 'y', 0)

    return id_same, id_free_x, id_u1_x, id_free_y, id_u1_y, id_free_p, id_u1_p, x_alpha, y_alpha, T

