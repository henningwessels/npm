"""
@author: Henning Wessels


"""

import numpy as np
import matplotlib.pyplot as plt
import math
import sys


from scipy.interpolate import griddata
import matplotlib.ticker as ticker

import scipy.io


testCase = 'sloshingIncomp'
t_max_step  = 55
start = 0

L = 1.0

x_min, x_max, y_min, y_max = 0.0, 1*L, 0.0, 1.5*L


for t_step in range(start, t_max_step):
    IC = np.load('IC_BC/' + testCase + '/IC_t' + str(t_step) + '.npy')


    x = IC[:, 0].flatten()[:, None]
    y = IC[:, 1].flatten()[:, None]
    pressure = IC[:, 4]


    '''Plot'''
    f, ax = plt.subplots(1)
    ax.axis('equal')
    plt.scatter(x, y, color='grey', s=40)

    # ax.tick_params(axis='both', labelsize=20)
    # major_ticks_x = np.arange(x_min, x_max + 1e-10, L)
    # major_ticks_y = np.arange(y_min, y_max + 1e-10, L)
    # ax.set_xticks(major_ticks_x)
    # ax.set_yticks(major_ticks_y)

    ax.set_xlim(x_min*0.9, x_max*1.1)
    ax.set_ylim(y_min*0.9, y_max*1.1)

    # plt.xlabel(r'$x$', fontsize=20)
    # plt.ylabel(r'$y$', fontsize=20)

    plt.gcf().subplots_adjust(bottom=0.2)
    plt.gcf().subplots_adjust(left=0.2)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 1.0, box.height])
    plt.axis('off')

    plt.savefig('figures/' + testCase + '/video/grid_t'+ str(t_step) + '.png')
    plt.close()


# for t_step in range(start, t_max_step):
#
#     '''Read data'''
#     IC = scipy.io.loadmat('IC_BC/' + testCase + '/IC_t' + str(t_step) + '.mat')['IC']
#     BC = scipy.io.loadmat('IC_BC/' + testCase + '/BC_t' + str(t_step) + '.mat')
#
#     x = IC[:, 0].flatten()[:, None]
#     y = IC[:, 1].flatten()[:, None]
#
#
#     id_u1_p = BC['id_u1_p']
#     coloring = np.zeros((len(y), 1))
#     coloring[id_u1_p, 0] = 1
#
#
#     '''Plot'''
#     f, ax = plt.subplots(1)
#     plt.scatter(x, y, c=coloring, s=40)
#     # plt.scatter(x[id_u1_p], y[id_u1_p], 'ro')
#
#     ax.tick_params(axis='both', labelsize=20)
#     major_ticks_x = np.arange(x_min, x_max + 1e-10, x_max)
#     major_ticks_y = np.arange(y_min, y_max + 1e-10, y_max)
#     ax.set_xticks(major_ticks_x)
#     ax.set_yticks(major_ticks_y)
#
#     ax.set_xlim(x_min*0.9, x_max*1.1)
#     ax.set_ylim(y_min*0.9, y_max*1.1)
#
#     plt.xlabel(r'$x$', fontsize=20)
#     plt.ylabel(r'$y$', fontsize=20)
#
#     plt.gcf().subplots_adjust(bottom=0.2)
#     plt.gcf().subplots_adjust(left=0.2)
#     box = ax.get_position()
#     ax.set_position([box.x0, box.y0, box.width * 1.0, box.height])
#     plt.grid()
#
#     plt.savefig('figures/' + testCase + '/video/p-boun_t'+ str(t_step) + '.png')
#     plt.close()
