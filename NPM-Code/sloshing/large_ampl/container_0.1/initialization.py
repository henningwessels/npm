"""
@author: Henning Wessels

"""


import numpy as np
import matplotlib.pyplot as plt
import math
import random



def plotInitialCondition(x, y, idu1, testCase, BC_type):

    U_plot = np.zeros((len(y), 1))
    for i in idu1:
        U_plot[i, 0] = 1

    f, ax = plt.subplots(1)

    plt.scatter(x, y, c=U_plot, cmap='RdBu_r')
    cb = plt.colorbar()
    for l in cb.ax.yaxis.get_ticklabels():
        # l.set_weight("bold")
        l.set_fontsize(20)

    ax.tick_params(axis='both', labelsize=20)

    plt.savefig('figures/' + testCase + '/' + BC_type + '.png')
    plt.close()



def sloshing(testCase):

    # spatial discretization
    x_min  = 0.0
    x_max  = 1.0
    y_min  = 0.0
    y_max  = 1.0
    num_el = 700
    n_boun = 100     # boundary nodes per facet

    dx = .2/math.sqrt(num_el + n_boun)

    # temporal discretization
    t_0     = 0.0
    t_end   = 1e-1
    t_steps = 100
    dt      = (t_end - t_0)/t_steps
    # t       = np.arange(t_0, t_end + 1e-16, dt).flatten()[:, None]

    '''Discretization'''

    n_facets = 4

    '''Top boundary'''

    a = 0.2     # sloshing amplitude
    b = 1.0      # container width
    h = 1.0      # container height

    x_top = np.zeros((n_boun, 1))
    y_top = np.zeros((n_boun, 1))

    x_top[:, 0] = np.random.uniform(x_min+dx, x_max-dx, n_boun)

    x_top[0,0]  = 0.0
    x_top[-1,0] = 1.0

    y_top[:, 0] = h - a*np.sin((x_top[:,0]-0.5)*math.pi/b)

    idu1_p = np.arange(0, n_boun, 1)
    u1_p = np.zeros((n_boun, 1))


    # add pressure condition

    '''1st boundary facet: u_x(x=0, y) = 0'''
    x1_x       = np.zeros((n_boun, 1))
    y1_x       = np.zeros((n_boun, 1))
    y1_x[:,0]  = np.random.uniform(y_min + dx, y_max + a - dx, n_boun)

    u1_x       = np.zeros((n_boun+2, 1)) # add two edges from top boundary

    idu1_x     = np.arange(n_boun, 2*n_boun, 1)     # index for later assemby of initial solution

    # add two edges from top boundary
    idu1_x     = np.insert(idu1_x,0,0)
    idu1_x     = np.insert(idu1_x,1,n_boun-1)

    '''2nd boundary facet: u_x(x=1, y) = 0'''
    x2_x       = np.ones((n_boun, 1))
    y2_x       = np.zeros((n_boun, 1))
    y2_x[:, 0] = np.random.uniform(y_min + dx, y_max - a - dx, n_boun)
    u2_x       = np.zeros((n_boun, 1))

    # append to x1, y1, u1
    x1_x       = np.append(x1_x, x2_x, axis=0)
    y1_x       = np.append(y1_x, y2_x, axis=0)
    u1_x       = np.append(u1_x, u2_x, axis=0)

    idu1_x     = np.append(idu1_x, np.arange(2*n_boun, 3*n_boun, 1)) # index for later assemby of initial solution


    '''3rd boundary facet: u_y(x, y=0) = 0'''
    x1_y       = np.zeros((n_boun, 1))
    x1_y[:, 0] = np.random.uniform(x_min + dx, x_max - dx, n_boun)
    y1_y       = np.zeros((n_boun, 1))
    u1_y       = np.zeros((n_boun, 1))

    idu1_y     = np.arange(3*n_boun, 4*n_boun, 1)

    x_boun = np.concatenate([x_top, x1_x, x1_y], axis=0)
    y_boun = np.concatenate([y_top, y1_x, y1_y], axis=0)

    '''Fill the empty hull with interior nodes'''

    x = np.random.uniform(dx, (1 - dx), num_el).flatten()[:, None]
    y = np.zeros(len(x), dtype=np.float32).flatten()[:, None]

    for i in range(0, len(x)):
        y_ub =  1.0 - a*np.sin((x[i]-0.5)*math.pi/b)
        y[i] = random.uniform(dx, y_ub-dx)

    # concatenate boundary and interior nodes
    x = np.concatenate([x_boun, x], axis=0).flatten()[:, None]
    y = np.concatenate([y_boun, y], axis=0).flatten()[:, None]


    '''Upper and lower bounds'''
    lb = np.array([np.min(x), np.min(y)])
    ub = np.array([np.max(x), np.max(y)])


    '''Initial values'''
    T = np.zeros((3, len(x)))

    # plot colormap
    f, ax = plt.subplots(1)
    U_plot = np.zeros((len(y), 1))

    for i in idu1_x:
        U_plot[i, 0] = 1

    for i in idu1_y:
        U_plot[i, 0] = 2

    for i in idu1_p:
        U_plot[i, 0] = 3

    plt.scatter(x, y, c=U_plot, cmap='RdBu_r')
    cb = plt.colorbar()
    for l in cb.ax.yaxis.get_ticklabels():
        # l.set_weight("bold")
        l.set_fontsize(20)

    ax.tick_params(axis='both', labelsize=20)
    major_ticks = np.arange(lb[0], ub[0] + 1e-10, 0.5)
    ax.set_xticks(major_ticks)
    ax.set_yticks(major_ticks)

    plt.show()

    plt.savefig('figures/' + testCase + '/boundaryConditions.png')
    plt.close()

    '''Indices of free nodes'''
    id_free_x = np.arange(0, num_el+n_boun*n_facets)
    id_free_x = np.delete(id_free_x, idu1_x)

    id_free_y = np.arange(0, num_el+n_boun*n_facets)
    id_free_y = np.delete(id_free_y, idu1_y)

    id_free_p = np.arange(0, num_el+n_boun*n_facets)
    id_free_p = np.delete(id_free_p, idu1_p)


    '''Define training data'''
    # N = int(0.8*len(x))


    return id_free_x, idu1_x, id_free_y, idu1_y, id_free_p, idu1_p, lb, ub, x, y, T