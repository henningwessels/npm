"""
@author: Henning Wessels

Postprocessing: Plot of sloshing amplitude and energy, comparison of different time steps

"""

import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.ticker


class OOMFormatter(matplotlib.ticker.ScalarFormatter):
    def __init__(self, order=0, fformat="%1.1f", offset=True, mathText=True):
        self.oom = order
        self.fformat = fformat
        matplotlib.ticker.ScalarFormatter.__init__(self,useOffset=offset,useMathText=mathText)
    def _set_orderOfMagnitude(self, nothing):
        self.orderOfMagnitude = self.oom
    def _set_format(self, vmin, vmax):
        self.format = self.fformat
        #if self._useMathText:
            #self.format = '$%s$' % matplotlib.ticker._mathdefault(self.format)




def plotEnergy(time, energy, color, label):

    # plt.plot(time[1:-1], energy[0, 1:-1], 'ro', color=color, label=label)
    # plt.plot(time[1:-1], energy[0, 1:-1], color=color, label=label, linewidth=3)
    # plt.ylabel('Pressure Energy')
    # plt.plot(time[1:-1], energy[1, 1:-1], 'ro', color=color, label=label)
    # plt.plot(time[1:-1], energy[1, 1:-1], color=color, label=label, linewidth=3)
    # plt.ylabel('Potential Energy')
    # plt.plot(time[1:-1], energy[2, 1:-1], 'ro', color=color, label=label)
    # plt.plot(time[1:-1], energy[2, 1:-1], color=color, label=label, linewidth=3)
    # plt.ylabel('Kinetic Energy [J/m$^3$]')
    # plt.plot(time[1:-1], energy[3, 1:-1], 'ro', color=color, label=label)
    plt.plot(time[1:-1], energy[3, 1:-1], color=color, label=label, linewidth=3)
    plt.ylabel('Total Energy [J/m$^3$]')


if __name__ == "__main__":


    # dt_all = [1e-1, 3e-1, 1e0]
    dt_all = [1e-1]
    testCase = 'sloshingIncomp'

    color = 'black'
    iter  = 0
    max_t = 10

    f, ax = plt.subplots(1, figsize=[10.4, 4.8])

    major_ticks = np.arange(0, max_t + 1e-10, 5)
    ax.set_xticks(major_ticks)
    # major_ticks_y = np.arange(-1e-2, 1e-2+1e-10, 5e-3)
    # ax.set_yticks(major_ticks_y)

    for dt in dt_all:


        '''Load time-amplitude graph'''
        sloshingAmplitude = np.load('container_' + str(dt) + '/IC_BC/' + testCase + '/sloshing.npy')
        for i in range(0, len(sloshingAmplitude[:, 2])):
            if sloshingAmplitude[i, 2]> max_t:
                sloshingAmplitude = sloshingAmplitude[0:i, :]
                break

        if iter==1:
            # color = 'grey'
            color='cadetblue'
        elif iter==2:
            color = 'burlywood'
            # color='olive'
        elif iter==3:
            color='burlywood'
        elif iter==4:
            color='grey'

        label = str(dt) + ' s'
        plt.plot(sloshingAmplitude[:, 2], sloshingAmplitude[:, 0], color=color, label=label, linewidth=3)
        plt.plot(sloshingAmplitude[:, 2], sloshingAmplitude[:, 1], color=color, linewidth=3)

        iter = iter + 1


    plt.grid()

    # print(f.figsize) #=[10.4, 4.8]

    plt.rc('font', **{'family': 'serif', 'serif': ['Times']})
    plt.rc('font', size=20)

    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., prop={'size': 20})

    plt.xlabel('Time [s]')
    plt.ylabel('Amplitude [m]')

    plt.gcf().subplots_adjust(bottom=0.2)
    plt.gcf().subplots_adjust(left=0.14)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.82, box.height])
    ax.yaxis.set_major_formatter(OOMFormatter(-2, "%.1f"))

    plt.savefig('analytical.png')
    plt.close()

#############################

    '''Plot energy'''


    f, ax = plt.subplots(1, figsize=[10.4, 4.8])

    # major_ticks = np.arange(0, max_t + 1e-10, 5)
    ax.set_xticks(major_ticks)

    color = 'black'
    iter = 0

    for dt in dt_all:

        '''Load time-amplitude graph'''
        sloshingAmplitude = np.load('container_' + str(dt) + '/IC_BC/' + testCase + '/sloshing.npy')
        for i in range(0, len(sloshingAmplitude[:, 2])):
            if sloshingAmplitude[i, 2] > max_t:
                sloshingAmplitude = sloshingAmplitude[0:i, :]
                break

        if iter==1:
            # color = 'grey'
            color='cadetblue'
        elif iter==2:
            color = 'burlywood'
            # color='olive'
        elif iter==3:
            color='burlywood'
        elif iter==4:
            color='grey'

        label = str(dt) + ' s'

        '''Compute energy'''

        energy = np.zeros((4, len(sloshingAmplitude[:, 2])))
        for t_step in range(1, len(energy[1, :])):
            '''Read model parameter from file'''
            # IC = np.load('IC_BC/' + testCase + '/IC_t' + str(t_step - 1) + '.npy')
            IC = np.load('container_' + str(dt) + '/IC_BC/' + testCase + '/IC_t' + str(t_step) + '.npy')

            '''Initial conditions'''
            x = IC[:, 0].flatten()[:, None]
            y = IC[:, 1].flatten()[:, None]
            u0 = IC[:, 2:5]
            rho0 = IC[:, 5].flatten()[:, None]
            p = IC[:, 6].flatten()[:, None]

            '''Total energy'''
            pressure_head = abs(np.sum(p))

            potential_head = 1.0 * np.sum(np.multiply(rho0, y))

            velo_square = np.multiply(u0[:, 0], u0[:, 0]) + np.multiply(u0[:, 1], u0[:, 1])
            velocity_head = 0.5 * np.sum(np.multiply(velo_square, rho0))

            '''Total energy at time step'''
            energy[0, t_step] = pressure_head
            energy[1, t_step] = potential_head
            energy[2, t_step] = velocity_head
            energy[3, t_step] = pressure_head + potential_head + velocity_head

        '''Print maximum variation'''
        print('dt = %.3f' % (dt))

        max_press = np.max(energy[0, 1:-1])
        min_press = np.min(energy[0, 1:-1])
        print('Delta E_press = %.5e' % ((max_press -min_press)/min_press))

        min_pot = np.min(energy[1, 1:-1])
        max_pot = np.max(energy[1, 1:-1])
        print('Delta E_pot = %.5e' % ((max_pot -min_pot)/min_pot))

        min_velo = np.min(energy[2, 1:-1])
        max_velo = np.max(energy[2, 1:-1])
        print('Delta E_velo = %.5e' % ((max_velo -min_velo)/min_velo), '\n')

        plotEnergy(sloshingAmplitude[:, 2], energy, color, label)

        iter = iter + 1

    plt.grid()

    plt.rc('font', **{'family': 'serif', 'serif': ['Times']})
    plt.rc('font', size=20)

    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., prop={'size': 20})

    plt.xlabel('Time [s]')

    plt.gcf().subplots_adjust(bottom=0.2)
    plt.gcf().subplots_adjust(left=0.125)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.80, box.height])
    # ax.yaxis.set_major_formatter(OOMFormatter(-2, "%.1f"))


    plt.savefig('energy.png')
    plt.close()

    plt.close()
