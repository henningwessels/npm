"""
@author: Henning Wessels

"""

import numpy as np
import matplotlib.pyplot as plt



def plotSloshing(solution, color, label, testCase):


    plt.plot(solution[:, 2], solution[:, 0], 'ro', color=color, markersize=5, label=label)
    plt.plot(solution[:, 2], solution[:, 1], 'ro', color=color, markersize=5)

    plt.plot(solution[:, 2], solution[:, 0], '--', color=color)
    plt.plot(solution[:, 2], solution[:, 1], '--', color=color)

    plt.grid()
    plt.legend()

    plt.xlabel('Time')
    plt.ylabel('Amplitude')

    plt.savefig('figures/' + testCase + '/amplitude.png')

    plt.close()


def get_amplitude(x, y, id_u1_p, sloshingAmplitude, dt, t_step, testCase):

    id_p_min = np.argmin(x[id_u1_p])
    id_p_max = np.argmax(x[id_u1_p])

    print('x_min = %.3f, y(x_min) = %.3f' % (x[id_u1_p[id_p_min]], y[id_u1_p[id_p_min]]))
    print('x_max = %.3f, y(x_max) = %.3f' % (x[id_u1_p[id_p_max]], y[id_u1_p[id_p_max]]))

    sloshingAmplitude[t_step, 0] = y[id_u1_p[id_p_min]] - 1.0  # amplitude
    sloshingAmplitude[t_step, 1] = y[id_u1_p[id_p_max]] - 1.0
    sloshingAmplitude[t_step, 2] = dt * t_step  # time

    color = 'black'
    label = str(dt)
    plotSloshing(sloshingAmplitude, color, label, testCase)

    np.save('IC_BC/' + testCase + '/sloshing.npy', sloshingAmplitude)

    return sloshingAmplitude


